Modulefile inclusions to set FOO_DIR and FOO_ROOT environment variables are
set in config/modules.yaml

The TCL template here has addtions (at end) to check existence of `include/`, 
`lib/`, `/lib64` and set `FOO_INC` etc environment variables accordingly.

**TODO**: add corresponding code for Lua modulefiles.
 
