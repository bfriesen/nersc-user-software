
The first thing to understand about Spack environments is that **they are
not environments in the usual sense of the word**. They don't define an 
environment - ie the set of things visible and available - for working in.
They are more like a purchase order ("build order" perhaps): they describe
**what Spack should do when `spack install` is run**:

- which packages Spack should ensure are installed (`spack.yaml` has the 
  list of partial specs being requested)
- the exact specification of each of those packages, eg which compiler to 
  use, which variants to set, which specific build of a required package
  should be used to satisfy each dependency. (`spack.lock` holds this. If
  there is no `spack.lock` then when `spack install` is called it will 
  first concretize the `spack.yaml` to generate one).
- the exact version of the package.py file (and associated patches etc)
  to use for each requested and required install (`.spack_env` holds these,
  captured when the environment is concretized, I think)

With this in mind, the `*-trial-installs/` directories here have `README.md`
files and helper scripts to guide consultants through creating a 
concrete environment ("build order"), executing it (building and installing
in locally) and posting the resulting installations to a buildcache. The 
swowner account can then execute the same build order, bypassing much of the 
compilation work by unpacking the installation from the buildcache.

**Please read the `README.md` files in the relevant environment directories
for specific instructions**
