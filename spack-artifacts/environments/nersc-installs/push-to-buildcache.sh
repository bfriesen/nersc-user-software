#!/bin/bash

if [[ -z $SPACK_ENV ]] ; then
  echo "activate the environment first! (spack env activate -d .)"
  exit 1
fi

target=${1:-nersc}
url=$(spack mirror list | awk "/^${target}/ {print \$2}")

# prevent an existing gpg-agent from trying to use gnome to get a 
# passphrase: (also, you should add "no-allow-external-cache" to 
# $HOME/.gnupg/gpg-agent.conf)
gpgconf --kill gpg-agent

#set -x
spack find --no-groups -v | awk '/^\w/ {print}' | while read -r spec ; do 
  spack buildcache create -af -m $target --only package "$spec"
done

#for s in $(spack find --no-groups -v | awk '/^\w/ {print}'); do
#  spack buildcache create -m $target --only package "$s"
#done
spack buildcache update-index -d $url 
