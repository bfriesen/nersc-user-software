#!/bin/bash

if [[ -z $SPACK_BASE ]]; then
  echo "load spack module first"
  exit 1
fi

if [[ -z $SPACK_ENV ]] ; then
  echo "activate the environment first! (spack env activate -d .)"
  exit 1
fi

if [[ $# -ne 1 ]] ; then
  echo "Usage: ./develop.sh name_of_package"
  exit 1
fi

pkg=$1
if ! spack info $pkg > /dev/null 2>&1 ; then
  echo "no such package known to (this version of) Spack."
  echo "You can create one with:"
  echo "spack create $pkg"
  exit 1
fi

builtin_pkg=${SPACK_ROOT}/var/spack/repos/builtin/packages/${pkg}
stopgap_pkg=${SPACK_ENV}/../../repos/${SPACK_VERSION}/nersc/packages/${pkg}

if [[ -e ${stopgap_pkg} ]]; then
  echo "a local stopgap version of package ${pkg} already exists"
  echo "you can edit it with:"
  echo "spack edit $pkg"
  echo "the local version is at ${stopgap_pkg} and has:"
  ls -l ${stopgap_pkg}
  echo ""
  echo "and the builtin version is at ${builtin_pkg} and has:"
  ls -l ${builtin_pkg}
  exit 1
fi

echo "copying ${pkg} from builtin repo as a starting point"
cp -vr ${builtin_pkg} ${stopgap_pkg} &&
echo "you can edit it with:" &&
echo "spack edit $pkg"


