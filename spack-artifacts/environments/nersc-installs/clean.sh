#!/bin/bash

if ! [[ $(realpath $PWD) =~ /nersc-installs$ ]] ; then
  echo "run this from the nersc-installs environment directory"
  exit 1
fi

if [[ -z $SPACK_BASE ]]; then
  echo "load spack module first"
  exit 1
fi

echo "stashing and selectively overwriting local changes"
# the first command fails if any tracked files have changed,
# thus triggering stash-apply
git diff-index --quiet HEAD -- || (git stash && git stash apply)
set -x
rm -r .spack-env
rm -r install
rm -rf modules lmod
rm config.yaml
ln -s config.yaml-trial config.yaml
rm -f packages.yaml compilers.yaml
ln -s ../../config/${SPACK_NERSC_SYSTEM}/packages.yaml .
ln -s ../../config/${SPACK_NERSC_SYSTEM}/compilers.yaml .
cat spack.yaml.empty > spack.yaml
cat spack.lock.empty > spack.lock
set +x

if [[ -e $HOME/.spack ]] ; then
  echo "Warning: $HOME/.spack found: any settings in that directory"
  echo "will have probably-unwanted effects on builds you do here."
  echo "If you didn't intentionally set things there, it is best to"
  echo "  rm -rf $HOME/.spack"
  echo "to clear them."
fi 

if ( spack env st | grep -q 'No active environment' ) ; then
  echo "activate this environment with:"
  echo "spack env activate -d ."
fi 

echo "currently on branch"
git branch --show-current
if [[ $(git branch --show-current) =~ ^(main|master)$ ]] ; then 
  echo "Recommended: make a branch to work in:"
  echo "git checkout -b ${USER}/${SPACK_NERSC_SYSTEM}-trial-installs-$(date +%y%m%d-%H%M%S)"
fi
