#!/bin/bash

if ! [[ $(realpath $PWD) =~ /nersc-installs$ ]] ; then
  echo "run this from the nersc-installs environment directory"
  exit 1
fi

if [[ -z $SPACK_BASE ]]; then
  echo "load spack module first"
  exit 1
fi

branch=$(git symbolic-ref --short HEAD)
if [[ $branch =~ ^(master|main)$ ]] && [[ "$1" != "-f" ]]; then
  echo "Warning: you probably want to be on the branch you committed to,"
  echo "not the main trunk. Add -f to override this warning"
  exit 1
fi

set -x
rm config.yaml
ln -s config.yaml-real config.yaml
rm -f packages.yaml compilers.yaml
ln -s ../../config/${SPACK_NERSC_SYSTEM}/packages.yaml .
ln -s ../../config/${SPACK_NERSC_SYSTEM}/compilers.yaml .
set +x

if [[ -e $HOME/.spack ]] ; then
  echo "Warning: $HOME/.spack/ found: any settings in that directory"
  echo "will have probably-unwanted effects on builds you do here."
  echo "If you didn't intentionally set things there, it is best to"
  echo "  rm -rf $HOME/.spack"
  echo "to clear them."
  echo ""
fi 

if [[ -e ./.spack-env ]] ; then
  echo "Warning: .spack-env/ found in this directory, probably left"
  echo "over from previous install. Packages held in repos there"
  echo "might affect the behavior of Spack in this environment."
  echo "Consider removing or hiding .spack-env/"
  echo ""
fi

if ( spack env st | grep -q 'No active environment' ) ; then
  echo "activate this environment with:"
  echo "spack env activate -d ."
fi 


