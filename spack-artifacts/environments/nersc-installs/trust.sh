#!/bin/bash

# gpg key dirs:
e4s=/global/common/software/spackecp/gpgkeys/
nersc=/global/cfs/cdirs/nstaff/www/spack/cache/gpgkeys/
perlmutter=/global/common/sw/spack/buildcache/gpgkeys/

for d in $e4s $nersc $perlmutter ; do
for key in $(find $d -name '*.pub') ; do
  echo "spack gpg trust $d/$key"
  spack gpg trust $d/$key
done
done
