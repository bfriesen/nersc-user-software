# When preparing to install software into /global/common/sw/install

- You should have already done a trial install in a copy of this environment
  in your own account, and committed the changed files to a git branch 
  (we'll call it `$my_branch`)

  - ideally, during that process you also pushed the things you built to
    a buildcache (this is not necessary, but will speed up the swowner 
    phase of the install)

- You should now be in a working copy of <https://gitlab.com/NERSC/nersc-user-software> 
  somewhere in the swowner account (**not** in `/global/common/sw/spack`)

- You should have checked out `$my_branch`, such that in this directory 
  you now see the `spack.yaml`, `spack.lock` generated during your trial
  install


