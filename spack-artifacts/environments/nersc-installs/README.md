# nersc-installa

This directory is a spack environment for consultants to install software
into `/global/common/sw/install`

TODO update this 

Use this directory **as yourself** (not swowner) to trial-and-debug using Spack
to install software for Cori. 

See further below for [How this environment works](#how-this-environment-works) and
[What to do when the normal procedure does not work](#what-to-do-when-the-normal-procedure-does-not-work).

## Normal build procedure

1. `git clone` or `git pull` the latest revision of this nersc-user-software 
   repo somewhere in your account. Make a branch to work in. 
   (`git checkout -b my_name/some_new_branch_name`)

2. `module load spack/0.16.1`

3. `./clean.sh` will put this directory into a good starting state for 
   attempting an install

4. Check what Spack thinks is needed to build the software: `spack spec -Il foo`.
   `[^]` means "already installed at `/global/common/sw/install`, `[+]` means 
   "already installed in this trial directory" (probably on your previous attempt).
   Look at the output to decide if you want to adjust any options. You can see 
   what Spack knows about the software with `spack info foo`.

   If the `spack spec -Il foo` output shows that Spack is determined to use the 
   wrong compiler, or trying to rebuild a dependency when you want it to use 
   already-installed software, see [What to do when the normal procedure does not work](#what-to-do-when-the-normal-procedure-does-not-work), below.

5. Attempt to build the software: `spack install -v foo`.

   **Tip:** If building for KNL while on a login node, there is a significant 
   chance that the `./configure` or `cmake` step will fail due to attempting to 
   run a KNL executable. Running the failing `spack install` on a KNL node 
   via `salloc` should get past this. 

   If the attempt fails, see [What to do when the normal procedure does not work](#what-to-do-when-the-normal-procedure-does-not-work), below.

6. Push the built software to the buildcache. This step is optional, but saves 
   time and effort later when installing as swowner (and if you had to build on 
   a KNL node, the buildcache might be the only way to get it installed, as login 
   nodes can't run KNL binaries and KNL nodes can't write to `/global/common/`).
   There is some [once-off setup](https://nersc.pages.nersc.gov/consulting/consulting-documentation/third-party-software-support/#once-off-setup) steps needed to push to the buildcache 
   for the first time.

   `./push-to-buildcache.sh` script will find things you installed here and 
   push them to the buildcache.

7. If you get to here and everything has worked, `git commit -a` and `git push` 
   your branch back to gitlab. Next check the instructions in `../cori-installs/`,
   `usgrsu swowner` (perhaps in another terminal window), and continue the 
   install procedure there.

## What to do when the normal procedure does not work

(TODO developing a package, updating compilers.yaml, updating packages.yaml, 
manual install as fallback option)

## How this environment works

- compilers.yaml and packages.yaml are links to the ones spack uses for cori, so
  if you need to edit them, the edits should get reflected globally

- what the clean.sh, prepare-to-develop.sh and push-to-buildcache.sh do

-rw-rw---- 1 sleak sleak  158 May  7 22:00 README.md
-rwxrwx--- 1 sleak sleak  846 May  9 10:00 clean.sh
lrwxrwxrwx 1 sleak sleak   32 May  9 10:38 compilers.yaml -> ../../config/cray/compilers.yaml
-rw-rw---- 1 sleak sleak 1527 May  9 09:55 config.yaml
lrwxrwxrwx 1 sleak sleak   31 May  9 10:38 packages.yaml -> ../../config/cray/packages.yaml
-rwxrwx--- 1 sleak sleak  328 May  7 22:00 push-to-buildcache.sh
-rw-rw---- 1 sleak sleak  198 May  8 08:01 repos.yaml
-rw-rw---- 1 sleak sleak  117 May  8 09:10 spack.lock
-rw-rw---- 1 sleak sleak  117 May  8 09:10 spack.lock.empty
-rw-rw---- 1 sleak sleak  104 May  8 09:09 spack.yaml
-rw-rw---- 1 sleak sleak  104 May  8 09:07 spack.yaml.empty
