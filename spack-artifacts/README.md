
When setting up a new version of Spack:

- make symlinks in `$spack/etc/spack` to the contents of the `config/` directory here.
  The new spack instance will then use the configuration managed in this repo.

- make symlinks in `$spack/var/spack/environments` to the `environments/$thing-installs/`
  directories here. This will make the `$thing-installs/` directories into named
  environments in the spack instance. Only `swowner` will be able to write to them.
  Don't symlink the `$thing-trial-installs/` environments though: the idea is 
  for consultants to use those directories as anonymous environments for making 
  sure a thing will build in their own account before trying to actually perform 
  the install as swowner.
