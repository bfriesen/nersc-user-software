# NERSC User Software

This repo contains artifact to aid in reproducing software builds at NERSC.

`manual-build-scripts/` contains scripts to help build software packages
without Spack.

`spack-environments/` has environments to support building and installing 
software at NERSC.

`software-support-list/` has the list of software support details for 
3rd-party software at or known to NERSC, and some utilities for querying and 
manipulating the list.

`dockerfiles/` has Dockerfiles for building containerized software targeting
NERSC systems.
