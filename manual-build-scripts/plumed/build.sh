#!/bin/bash
#set -e
target=${target:=haswell}
prgenv=${prgenv:=gnu}
version=${version:=2.4.2}
prefix_root=${prefix_root:=~/opt}
name=plumed2
#
prefix=$prefix_root/$name/$version/$prgenv/$target
filename=$name-$version.tar.gz
# download
if ! [ -e $SCRATCH/.cache/$filename ]; then
    mkdir -p $SCRATCH/.cache
    url=https://github.com/plumed/plumed2/archive/v$version.tar.gz
    wget $url -O $SCRATCH/.cache/$filename
fi
# env
module list
# unload hugepages
#module unload $(module -l list 2>&1 | grep craype-hugepages | awk '{print $1}')
#
module swap PrgEnv-${PE_ENV,,} PrgEnv-$prgenv
export CRAYPE_LINK_TYPE=dynamic
# setup build dir
#case "$target" in
#    "mic-knl")
#	build_dir=$(mktemp -d --tmpdir=$SCRATCH/.build $name.XXXXXXX)
#        ;;
#    *)
	build_dir=$(mktemp -d --tmpdir $name.XXXXXXX)
#        ;;
#esac
cd $build_dir
tar zxf $SCRATCH/.cache/$filename
cd $(basename $SCRATCH/.cache/$filename .tar.gz)
# use login node compatible env for configure
module swap craype-${CRAY_CPU_TARGET} craype-haswell
module list
./configure --prefix=$prefix CC=cc CXX=CC FC=ftn F77=ftn \
    --disable-openmp
# switch to real env and build
module swap craype-${CRAY_CPU_TARGET} craype-$target
module list
nice make -j 16
make install
rm -fr $build_dir
