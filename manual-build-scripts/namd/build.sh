#!/bin/bash
#set -e
target=${target:=haswell}
prgenv=${prgenv:=intel}
version=${version:=2.13}
prefix_root=${prefix_root:=~/opt}
name=namd
smp=${smp:=false}
#
prefix=$prefix_root/$name/$version/$target
if [ "$smp" = true ]; then
    prefix=$prefix_root/$name/$version-smp/$target
fi
filename=NAMD_$version'_Source'.tar.gz
#
if ! [ -e $SCRATCH/.cache/$filename ]; then
    echo $SCRATCH/.cache/$filename 'not found'
    exit 1
fi
#
module list
module unload $(module -l list 2>&1 | grep craype-hugepages | awk '{print $1}')
module load craype-hugepages8M
module load cray-fftw
module load rca
module swap PrgEnv-${PE_ENV,,} PrgEnv-$prgenv
module swap craype-${CRAY_CPU_TARGET} craype-${target}
module swap intel intel/18.0.3.222
module list
build_dir=$(mktemp -d --tmpdir $name.XXXXXXX)
script_dir=$(pwd)
cd $build_dir
tar zxf $SCRATCH/.cache/$filename
cd $(basename $SCRATCH/.cache/$filename .tar.gz)

wget http://www.ks.uiuc.edu/Research/namd/libraries/tcl8.5.9-linux-x86_64.tar.gz
wget http://www.ks.uiuc.edu/Research/namd/libraries/tcl8.5.9-linux-x86_64-threaded.tar.gz
tar xzf tcl8.5.9-linux-x86_64.tar.gz & 
tar xzf tcl8.5.9-linux-x86_64-threaded.tar.gz &
wait
mv tcl8.5.9-linux-x86_64 tcl
mv tcl8.5.9-linux-x86_64-threaded tcl-threaded

ls
# build charm
tar xf charm-6.8.2.tar
cd charm-6.8.2
if [ "$smp" = true ]; then
    ./build charm++ gni-crayxc persistent smp --with-production -j8
else
    ./build charm++ gni-crayxc persistent --with-production -j8
fi
cd ..
#
if [[ $target == mic-knl ]]; then
    arch=CRAY-XC-KNL-intel
else
    arch=CRAY-XC-intel
fi
#
if [ "$smp" = true ]; then
    ./config $arch --with-fftw3 --charm-arch gni-crayxc-persistent-smp
else
    ./config $arch --with-fftw3 --charm-arch gni-crayxc-persistent    
fi
cd $arch
gmake -j 8
make release

mkdir -p $prefix
cp -r NAMD*/* $prefix/

rm -fr $build_dir
