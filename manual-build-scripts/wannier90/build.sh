#!/bin/bash
#set -e
target=${target:=haswell}
prgenv=${prgenv:=intel}
version=${version:=3.0.0}
prefix_root=${prefix_root:=~/opt}
name=wannier90
prefix=$prefix_root/$name/$version/$prgenv
filename=$name-$version.tar.gz
if ! [ -e $SCRATCH/.cache/$filename ]; then
    mkdir -p $SCRATCH/.cache
    url=https://github.com/wannier-developers/wannier90/archive/v$version.tar.gz
    wget $url -O $SCRATCH/.cache/$filename
fi
pkgdir=$(pwd)
module list
module swap PrgEnv-${PE_ENV,,} PrgEnv-$prgenv
module swap craype-${CRAY_CPU_TARGET} craype-$target
module unload cray-libsci
build_dir=$(mktemp -d --tmpdir=$SCRATCH/.build $name.XXXXXXX)
cd $build_dir
tar zxf $SCRATCH/.cache/$filename
cd $(basename $SCRATCH/.cache/$filename .tar.gz)
module list
cp $pkgdir/make.inc.cori-intel make.inc
make -j 8
make -j 8 lib
if [ $? -eq 0 ]; then
    echo 'OK'
    mkdir -p $prefix
    mkdir -p $prefix/lib
    cp *.x $prefix/
    cp libwannier.a $prefix/lib/
else
    echo 'ERROR! build in: '$build_dir
fi
