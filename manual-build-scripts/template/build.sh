#!/bin/bash
#set -e
target=${target:=haswell}
prgenv=${prgenv:=intel}
version=${version:=}
prefix_root=${prefix_root:=~/opt}
name=
#
prefix=$prefix_root/$name/$version/$prgenv/$target
filename=$name-$version.tar.gz
# download
if ! [ -e $SCRATCH/.cache/$filename ]; then
    mkdir -p $SCRATCH/.cache
    url=
    wget $url -O $SCRATCH/.cache/$filename
fi
# env
module list
# unload hugepages
module unload $(module -l list 2>&1 | grep craype-hugepages | awk '{print $1}')
#
module swap PrgEnv-${PE_ENV,,} PrgEnv-$prgenv
# expects dynamic linking by default?
#export CRAYPE_LINK_TYPE=dynamic
# setup build dir
# build on compute mounted file system if cross compiles are broken (usually the case)
case "$target" in
    "mic-knl" | "ivybridge")
	build_dir=$(mktemp -d --tmpdir=$SCRATCH/.build $name.XXXXXXX)
        ;;
    *)
	build_dir=$(mktemp -d --tmpdir $name.XXXXXXX)
        ;;
esac

cd $build_dir
tar zxf $SCRATCH/.cache/$filename
cd $(basename $SCRATCH/.cache/$filename .tar.gz)
# compiler options
case "$prgenv" in
    intel)
	FLAGS="-O2"
	;;
    gnu)
	FLAGS="-O3"
	;;
    cray)
	FLAGS="-O1"
	;;
esac
# use login node compatible env for configure
case "$NERSC_HOST" in
    edison)
	module swap craype-${CRAY_CPU_TARGET} craype-sandybridge
        ;;
    cori)
	module swap craype-${CRAY_CPU_TARGET} craype-haswell
        ;;
    *)
	module swap craype-${CRAY_CPU_TARGET} craype-sandybridge
	;;
esac
module list
./configure --prefix=$prefix CC=cc FC=ftn F77=ftn\
    CFLAGS="$FLAGS" FCFLAGS="$FLAGS" \
    --enable-shared \
    --enable-static
# switch to real env and build
module swap craype-${CRAY_CPU_TARGET} craype-$target
module list
# try to nicely use 1/2 the node for the build
nice make -j -l $(( $(nproc) / 2 ))
# run tests on login node if possible 
case "$target" in
    mic-knl)
	srun -n 1 -C knl -t 30 --qos=debug make -j 66 check
	;;
    ivybridge)
	srun -n 1 -t 30 --qos=debug make -j 24 check
	;;
    *)
	nice make -j -l $(( $(nproc) / 2 )) check
	;;
esac
if [ $? -eq 0 ]; then
    make install
    rm -r $build_dir
else
    echo 'ERROR! build in: '$build_dir
fi
