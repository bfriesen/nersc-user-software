# NERSC user software

1. write your spec/ build script/ module file
2. PR to devel branch
3. CI builds specs/ run tests/ etc
4. do real production build to /usr/common/ with tags + master branch for well defined "NERSC software release"
5. Use spack mechanisms to deploy /usr/common/software/modulefiles
6. For non-spack packages copy the modules files over