#!/bin/bash
set -e
target=${target:=haswell}
prgenv=${prgenv:=intel}
version=${version:=9.4.1}
name=abinit
prefix_root=${prefix_root:=~/opt}
#
prefix=$prefix_root/$name/$version/$prgenv
filename=$name-$version.tar.gz
# download
if ! [ -e $SCRATCH/.cache/$filename ]; then
    mkdir -p $SCRATCH/.cache
    url=https://www.abinit.org/sites/default/files/packages/$filename
    wget $url -O $SCRATCH/.cache/$filename
fi
pkgdir=$(pwd)
module swap PrgEnv-${PE_ENV,,} PrgEnv-$prgenv
module swap craype-${CRAY_CPU_TARGET} craype-$target
module unload cray-libsci
module load cray-hdf5-parallel
module load cray-netcdf-hdf5parallel
module list
build_dir=$(mktemp -d --tmpdir=/tmp $name.$version.XXXXXXX)
cd $build_dir
tar xzf $SCRATCH/.cache/$filename
cd $name-$version
module list
FLAGS='-mkl=sequential -axHASWELL,KNL'
./configure --prefix=$prefix \
    CC="cc" CXX="CC" FC="ftn" \
    CFLAGS="$FLAGS" \
    CXXFLAGS="$FLAGS" \
    FCFLAGS="$FLAGS" \
    MPI_LIBS='-lmpich_intel -lmpichf90_intel' \
    --with-mpi=yes \
    --with-mpi-flavor=native \
    --with-linalg-flavor='mkl' \
    --with-libxc=/usr/common/software/libxc/5.1.0/gnu/haswell \
    --with-netcdf --with-netcdf-fortran \
    --with-hdf5
make -j 8
if [ $? -eq 0 ]; then
    make install
fi
rm -r $build_dir
