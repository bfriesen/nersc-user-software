#!/bin/bash
salloc


#SBATCH -C haswell
#SBATCH --nodes=1
#SBATCH --time=30
#SBATCH --qos=debug

module list

./runtests.py -j 32
#./runtests.py paral -n 10 --use-srun
#./runtests.py paral -n 4 --use-srun
