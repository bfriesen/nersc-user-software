#!/bin/bash
set -e
target=${target:=haswell}
prgenv=${prgenv:=intel}
version=${version:=8.10.3}
name=abinit
prefix_root=${prefix_root:=~/opt}
#
prefix=$prefix_root/$name/$version/$prgenv
filename=$name-$version.tar.gz
# download
if ! [ -e $SCRATCH/.cache/$filename ]; then
    mkdir -p $SCRATCH/.cache
    url=https://www.abinit.org/sites/default/files/packages/$filename
    wget $url -O $SCRATCH/.cache/$filename
fi
pkgdir=$(pwd)
export CRAYPE_LINK_TYPE=dynamic
module load craype-hugepages2M
module swap PrgEnv-${PE_ENV,,} PrgEnv-$prgenv
module swap craype-${CRAY_CPU_TARGET} craype-$target
module unload cray-libsci
module list
build_dir=$(mktemp -d --tmpdir=/tmp $name.$version.XXXXXXX)
cd $build_dir
tar xzf $SCRATCH/.cache/$filename
cd $name-$version
module list
FLAGS='-mkl=sequential -axHASWELL,KNL'
./configure --prefix=$prefix \
    CC="cc" CXX="CC" FC="ftn" \
    CFLAGS="$FLAGS" \
    CXXFLAGS="$FLAGS" \
    FCFLAGS="$FLAGS" \
    --with-trio-flavor='netcdf' \
    --with-dft-flavor='libxc+wannier90+atompaw' \
    --with-linalg-flavor='mkl' \
    --with-fc-vendor='intel'
make -j 8
if [ $? -eq 0 ]; then
    make install
    cp -r $build_dir/$name-$version/fallbacks/exports $prefix/extras
fi
rm -r $build_dir
