#!/bin/bash
set -e
target=haswell
prgenv=gnu
version=1.1.4
name=libint
prefix_root=${prefix_root:=~/opt}
#
prefix=$prefix_root/$name/$version/$prgenv/$target
filename=$name-$version.tar.gz
# download
if ! [ -e $SCRATCH/.cache/$filename ]; then
    mkdir -p $SCRATCH/.cache
    url=http://sourceforge.net/projects/libint/files/v1-releases/$filename
    wget $url -O $SCRATCH/.cache/$filename
fi
# env
# unload hugepages
#module unload $(module -l list 2>&1 | grep craype-hugepages | awk '{print $1}')
#
module swap PrgEnv-${PE_ENV,,} PrgEnv-$prgenv
module swap craype-${CRAY_CPU_TARGET} craype-$target
export CRAYPE_LINK_TYPE=dynamic
module list
# setup build dir
build_dir=$(mktemp -d --tmpdir libint.XXXXXXX)
cd $build_dir
tar zxf $SCRATCH/.cache/$filename
cd $(basename $filename .tar.gz)
# configure/make/install
aclocal -I lib/autoconf
autoconf
./configure --prefix=$prefix CC=cc CXX=CC F77=ftn
nice make -j -l $(( $(nproc) / 2 ))
if [ $? -eq 0 ]; then
    make install
    rm -r $build_dir
fi
