#!/bin/bash
set -e
target=${target:=haswell}
prgenv=gnu
version=2.6.0
name=libint
prefix_root=${prefix_root:=~/opt}
#
prefix=$prefix_root/$name/$version/$prgenv/$target
filename=libint-v$version-cp2k-lmax-4.tgz
# download
if ! [ -e $SCRATCH/.cache/$filename ]; then
    mkdir -p $SCRATCH/.cache
    url=https://github.com/cp2k/libint-cp2k/releases/download/v2.6.0/$filename
    wget $url -O $SCRATCH/.cache/$filename
fi
module swap PrgEnv-${PE_ENV,,} PrgEnv-$prgenv
module swap craype-${CRAY_CPU_TARGET} craype-$target
module load cmake/3.18.2
export CRAYPE_LINK_TYPE=dynamic
module list
# setup build dir
build_dir=$(mktemp -d --tmpdir libint.XXXXXXX)
cd $build_dir
tar zxf $SCRATCH/.cache/$filename
cd $(basename $filename .tgz)

export FC=ftn
cmake . \
  -DCMAKE_INSTALL_PREFIX=$prefix \
  -DCMAKE_CXX_COMPILER=CC \
  -DENABLE_FORTRAN=ON

make -j 16
if [ $? -eq 0 ]; then
    make install
    rm -r $build_dir
fi
