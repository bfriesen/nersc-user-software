#!/bin/bash
#set -e
target=${target:=haswell}
prgenv=${prgenv:=intel}
version=${version:=1.12.1}
prefix_root=${prefix_root:=~/opt}
name=libxsmm
#
prefix=$prefix_root/$name/$version/$prgenv/$target
filename=$name-$version.tar.gz
# env
module rm altd darshan
module list
# unload hugepages
#module unload $(module -l list 2>&1 | grep craype-hugepages | awk '{print $1}')
#
module swap PrgEnv-${PE_ENV,,} PrgEnv-$prgenv
# setup
build_dir=$(mktemp -d --tmpdir $name.XXXXXXX)
cd $build_dir
# download
url=https://github.com/hfp/libxsmm.git
git clone --branch $version $url
cd libxsmm
git log | head
# build
module list
case "$target" in
    haswell)	
	make CXX=CC CC=cc FC=ftn AVX=2 OPT=3 PREFIX=${prefix} install
	;;
    mic-knl)	
	make CXX=CC CC=cc FC=ftn AVX=3 MIC=1 OPT=3 PREFIX=${prefix} install
	;;
    *)
	echo "unknown target!"
	exit 1
	;;
esac
rm -rf $build_dir
