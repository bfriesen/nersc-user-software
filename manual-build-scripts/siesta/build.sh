#!/bin/bash
set -e
set -x
target=${target:=haswell}
prgenv=${prgenv:=intel}
version=${version:=4.1.5}
prefix_root=${prefix_root:=~/opt}
name=siesta
#
prefix=$prefix_root/$name/$version/$prgenv
filename=$name-$version.tar.gz
#
if ! [ -e $SCRATCH/.cache/$filename ]; then
    mkdir -p $SCRATCH/.cache
    # starting with release 4.1.5    
    url=https://gitlab.com/siesta-project/siesta/-/releases/v$version/downloads/$filename    
    #vb=$(echo ${version%-*} | cut -f1,2 -d'.') #strip suffix starting with "-" and only keep x.y
    #url=https://launchpad.net/siesta/$vb/$version/+download/siesta-$version.tar.gz
    wget $url -O $SCRATCH/.cache/$filename
fi
module list
module swap PrgEnv-${PE_ENV,,} PrgEnv-$prgenv
# 19.0.3 has ICE on 4.1-b4
#module swap intel intel/18.0.3.222
module swap intel/19.1.3.304
build_dir=$(mktemp -d --tmpdir $name.XXXXXXX)
script_dir=$(pwd)
cd $build_dir
tar zxf $SCRATCH/.cache/$filename
cd $(basename $SCRATCH/.cache/$filename .tar.gz)

cd Obj
sh ../Src/obj_setup.sh
cp $script_dir/arch.make .
make

mkdir -p $prefix
cp ./siesta $prefix/siesta

make clean
make clean-transiesta
make transiesta
cp ./transiesta $prefix/transiesta

#rm -r $build_dir
#rm -r /tmp/$name.*
