#!/bin/bash
#set -e
target=${target:=haswell}
prgenv=${prgenv:=intel}
version=${version:=1.72.0}
prefix_root=${prefix_root:=~/opt}
name=boost
#
prefix=$prefix_root/$name/$version/$prgenv/$target
filename=$name"_"$(echo "$version" | tr "." "_").tar.bz2
# download
if ! [ -e $SCRATCH/.cache/$filename ]; then
    mkdir -p $SCRATCH/.cache
    url=https://sourceforge.net/projects/boost/files/boost/$version/$filename
    wget $url -O $SCRATCH/.cache/$filename
fi
# env
module swap PrgEnv-${PE_ENV,,} PrgEnv-$prgenv
module swap craype-${CRAY_CPU_TARGET} craype-$target
if [ "$prgenv" == "intel" ]; then
    # boost 1.72.0 not tested against intel/19.*
    module swap intel intel/18.0.3.222
fi
module load python
module list
build_dir=$(mktemp -d --tmpdir $name.XXXXXXX)
cd $build_dir
tar jxf $SCRATCH/.cache/$filename
cd $(basename $SCRATCH/.cache/$filename .tar.bz2)
# compiler options
case "$prgenv" in
    intel)
	toolset=intel-linux
	;;
    gnu)
	toolset=gcc
	;;
    cray)
	toolset=cray
	;;
esac

### make custom jam for MPI
if [[ -f "$HOME/user-config.jam" ]]; then
    echo "$HOME/user-config.jam exists"
    set -e
    tmp=$(mktemp --tmpdir=$SCRATCH/.trash user-config.jam.XXXXXX)
    mv $HOME/user-config.jam $tmp
    set +e
fi


cat << END > $HOME/user-config.jam
using python
: 3.7
: /usr/common/software/python/3.7-anaconda-2019.10/bin/python
: /usr/common/software/python/3.7-anaconda-2019.10/include/python3.7m
: /usr/common/software/python/3.7-anaconda-2019.10/lib/
;

using mpi : :
  <include>$MPICH_DIR/include
  <library-path>$MPICH_DIR/lib
END

cat << END > test.cxx
int main() {
  return 0;
}
END

mpich_libs=$(CC -v test.cxx 2>&1 \
    | tr -s ' ' '\n' \
    | grep ^-lmpich \
    | sed 's/^-l//')

for lib in $mpich_libs; do
    echo "  <find-shared-library>$lib " >> $HOME/user-config.jam
done
echo "  ;" >> $HOME/user-config.jam
rm -f test.cxx a.out

echo "note: wrote custom mpi config to $HOME/user-config.jam"
custom_jam=$HOME/user-config.jam

### end custom JAM for MPI

./bootstrap.sh --prefix=$prefix --with-libraries=all

# no cmake config due to bugs with CMake + boost/1.70.0
nice ./b2 install --toolset=$toolset -j 20 #--no-cmake-config

rm -r $build_dir
