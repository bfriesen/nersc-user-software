#!/usr/bin/env bash

# First build the non-hdf5 version.
./build_generic.sh

# Then build the hdf5 version.
hdf5_module="cray-hdf5-parallel/1.10.5.2" ./build_generic.sh
