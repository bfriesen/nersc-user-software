#!/usr/bin/env bash
set -e #-x

# Always target haswell so that is works also on KNL, and gcc so that
# no runtime dependency are needed, e.g. intel math libs become
# dependencies with PrgEnv-intel, but no need to optimize darshan.
target=haswell
prgenv=gnu

name=darshan
version=${version:=3.2.1}
prefix_root=${prefix_root:=/usr/common/software}

archive_ext="tar.gz"
filename=$name-$version.$archive_ext
url="https://ftp.mcs.anl.gov/pub/darshan/releases/${filename}"

if ! [ -e $SCRATCH/.cache/$filename ]; then
    mkdir -p $SCRATCH/.cache
    wget -L -O $SCRATCH/.cache/$filename $url
fi

module list
module swap PrgEnv-${PE_ENV,,} PrgEnv-$prgenv
build_dir=$(mktemp -d --tmpdir $name.XXXXXXX)

pushd $build_dir
tar zxf $SCRATCH/.cache/$filename

# Use login node compatible env for configure.
module swap craype-${CRAY_CPU_TARGET} craype-haswell

if [ -z ${hdf5_module+x} ]; then
    prefix=$prefix_root/$name/$version
else
    # Set the hdf5 configure option only when the hdf5 module var is defined.
    module load $hdf5_module
    OTHER_OPTS="--enable-hdf5-mod=${HDF5_ROOT}"
    prefix=$prefix_root/$name/${version}-hdf5
fi

module list

# Configure both the runtime and the utilities with the same config.

pushd $(basename $SCRATCH/.cache/$filename .${archive_ext})/darshan-runtime
./configure --prefix=$prefix \
    --with-mem-align=8 \
    --with-log-path=/global/cscratch1/sd/darshanlogs \
    --enable-mmap-logs \
    --with-jobid-env=SLURM_JOB_ID \
    --disable-cuserid \
    --enable-group-readable-logs \
        $OTHER_OPTS CFLAGS='-fPIC -O3' CC=cc
popd

pushd $(basename $SCRATCH/.cache/$filename .${archive_ext})/darshan-util
./configure --prefix=$prefix \
        CFLAGS='-fPIC -O3' CC=cc
popd


# Switch to the real env and build both the runtime and the utilities.

module swap craype-${CRAY_CPU_TARGET} craype-$target
module list

pushd $(basename $SCRATCH/.cache/$filename .${archive_ext})/darshan-runtime
nice make -j 16; make install
popd

pushd $(basename $SCRATCH/.cache/$filename .${archive_ext})/darshan-util
nice make -j 16; make install
popd

popd; rm -fr $build_dir

fix_perms -g swowner $prefix
