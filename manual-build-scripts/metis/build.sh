#!/bin/bash
#set -e
target=${target:=haswell}
prgenv=${prgenv:=intel}
version=${version:=5.1.0}
prefix_root=${prefix_root:=~/opt}
name=metis
#
prefix=$prefix_root/$name/$version/
filename=$name-$version.tar.gz
# download
if ! [ -e $SCRATCH/.cache/$filename ]; then
    mkdir -p $SCRATCH/.cache
    url=http://glaros.dtc.umn.edu/gkhome/fetch/sw/metis/$filename
    wget $url -O $SCRATCH/.cache/$filename
fi
module list
module swap PrgEnv-${PE_ENV,,} PrgEnv-$prgenv
build_dir=$(mktemp -d --tmpdir $name.XXXXXXX)
cd $build_dir
tar zxf $SCRATCH/.cache/$filename
cd $(basename $filename .tar.gz)
module list
make config prefix=$prefix cc=gcc #shared=1 
nice make -j
make install
rm -fr $build_dir
rm -fr /tmp/$name.*
