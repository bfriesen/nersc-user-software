#!/bin/bash
#SBATCH -q debug
#SBATCH -A nstaff
#SBATCH -t 00:30:00
#SBATCH -C haswell
#SBATCH -J cp2k-test-n2o5
#SBATCH --switches=1@600
#SBATCH --ntasks-per-node=16
#SBATCH --cpus-per-task=4
d=run.$SLURM_JOBID
mkdir $d && cd $d
for f in POTENTIAL_revPBE  conf.xyz  n2o5.inp; do
    cp ../$f .
done
export OMP_NUM_THREADS=2
srun --cpu-bind=cores ~/opt/cp2k/6.1/haswell/cp2k.psmp n2o5.inp 2>&1 | tee n2o5.out
