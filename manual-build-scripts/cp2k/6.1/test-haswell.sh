#!/bin/bash
#SBATCH -C haswell
#SBATCH -t 60
#SBATCH --qos=regular
#SBATCH -J cp2k-test-suite
base=$(pwd)
cp2k_rel=$(realpath --relative-to="." $base)
cp2k_run_prefix="srun -n 32 -c 2 --cpu-bind=cores " $base/tools/regtesting/do_regtest \
    -nobuild -nosvn -farming -arch $1 -version popt \
    -cp2kdir $cp2k_rel -maxtasks 32 -jobmaxtime 6000
