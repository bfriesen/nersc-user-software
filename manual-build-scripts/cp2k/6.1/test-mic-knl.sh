#!/bin/bash
#SBATCH -C knl
#SBATCH -t 60
#SBATCH --qos=regular
#SBATCH -J cp2k-test-suite
base=$(pwd)
cp2k_rel=$(realpath --relative-to="." $base)
cp2k_run_prefix="srun -n 66 -c 4 --cpu-bind=cores " $base/tools/regtesting/do_regtest \
    -nobuild -nosvn -farming -arch $1 -version popt \
    -cp2kdir $cp2k_rel -maxtasks 66 -jobmaxtime 6000
