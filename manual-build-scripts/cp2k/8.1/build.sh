#!/bin/bash
target=${target:=haswell}
prgenv=gnu
version=8.1
name=cp2k
prefix_root=${prefix_root:=~/opt}
#
filename=$name-$version.tar.bz2
# download
if ! [ -e $SCRATCH/.cache/$filename ]; then
    mkdir -p $SCRATCH/.cache
    url=https://github.com/cp2k/cp2k/releases/download/v${version}.0/$filename
    wget $url -O $SCRATCH/.cache/$filename
fi
pkgdir=$(pwd)
export CRAYPE_LINK_TYPE=static
module unload altd
module swap PrgEnv-${PE_ENV,,} PrgEnv-$prgenv
module swap craype-${CRAY_CPU_TARGET} craype-$target
module load cray-fftw
module load cray-libsci
module unload $(module -l list 2>&1 | grep craype-hugepages | awk '{print $1}')
module load craype-hugepages2M
module list
build_dir=$(mktemp -d --tmpdir=/tmp cp2k.XXXXXXX)
cd $build_dir
tar xjf $SCRATCH/.cache/$filename
cd cp2k-$version
arch=cori-gnu
cp $pkgdir/$arch.* arch/
make -j ARCH=$arch VERSION=psmp
prefix=$prefix_root/$name/$version/$target
mkdir -p $prefix
cp exe/$arch/* $prefix/
