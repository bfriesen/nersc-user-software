#!/bin/bash
set -e
target=${target:=haswell}
prgenv=${prgenv:=gnu}
version=${version:=4.3.4}
prefix_root=${prefix_root:=~/opt}
name=libxc
#
prefix=$prefix_root/$name/$version/$prgenv/$target
filename=$name-$version.tar.gz
# download
url=https://gitlab.com/libxc/libxc
module list
module swap PrgEnv-${PE_ENV,,} PrgEnv-$prgenv
export CRAYPE_LINK_TYPE=dynamic
build_dir=$(mktemp -d --tmpdir $name.XXXXXXX)
cd $build_dir
git clone --branch $version $url
cd $name
# compiler options
case "$prgenv" in
    intel)
	FLAGS="-O3 -fp-model strict"
	;;
    gnu)
	FLAGS="-O3"
	;;
    cray)
	FLAGS="-O1 -hfp0"
	;;
esac
# use login node compatible env for configure
module swap craype-${CRAY_CPU_TARGET} craype-haswell
module list
autoreconf -i
./configure --prefix=$prefix CC=cc FC=ftn F77=ftn\
    CFLAGS="$FLAGS" FCFLAGS="$FLAGS" \
    --enable-shared \
    --enable-static
# switch to real env and build
module swap craype-${CRAY_CPU_TARGET} craype-$target
module list
make -j 16
if [ $? -eq 0 ]; then
    make install
fi
rm -fr $build_dir
rm -fr /tmp/$name.*
