This image builds on <https://hub.docker.com/repository/docker/sleak75/groovy-mpich34-haswell> by 
adding miniconda, mpi4py and numpy.

This image can be found at <https://hub.docker.com/repository/docker/sleak75/conda-mpi4py-haswell>
and at NERSC with `shifter --image=sleak75/conda-mpi4py-haswell:latest /bin/bash`

The intent of this image is for use as a base for parallel python workloads. A Dockerfile
using this might look like:

    FROM sleak75/conda-mpi4py-haswell:latest 
    SHELL ["/bin/bash", "-c"]
    WORKDIR /app

    RUN conda install foo 
    # do this to reduce image size:
    RUN conda clean -a

    RUN /sbin/ldconfig

    COPY my_python_script.py ./
    RUN chmod -R a+rX /app


