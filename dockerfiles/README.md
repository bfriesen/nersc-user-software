The subdirectories here have Dockerfiles corresponding to images available at
NERSC from the Shifter Image Gateway (<https://docs.nersc.gov/development/shifter/>)
as well as on DockerHub.

Some directories will describe ready-to-use applications, but most are intended
to provide an easy-to-use base for customized container-based workflows. A
Dockerfile using one of these might look like:

    FROM sleak75/conda-mpi4py-haswell:latest 
    SHELL ["/bin/bash", "-c"]
    WORKDIR /app

    RUN conda install foo 
    # do this to reduce image size:
    RUN conda clean -a

    RUN /sbin/ldconfig

    COPY my_python_script.py ./
    RUN chmod -R a+rX /app

This can be prepared and tested on a local workstation 
(`mylaptop$ docker build -t fred/myapp:latest`), (where `fred` is my DockerHub 
user name), pushed to DockerHub (`mylaptop$ docker push fred/myapp:latest`), 
pulled to the Shifter Image Gateway (`cori$ shifterimg pull fred/myapp:latest`)
and used in a job (`sbatch --image=docker:fred/myapp:latest myjob.sh`).

See <https://docs.nersc.gov/development/shifter/how-to-use/> for more about 
running jobs in Shifter at NERSC.

