This image is available on Dockerhub at <https://hub.docker.com/repository/docker/sleak75/groovy-mpich34-haswell> and also at NERSC
with `shifter --image docker:sleak75/groovy-mpich34-haswell /bin/bash`

See the Dockerfile in `../conda-mpi4py-haswell` for an example of building a 
container on top of this image
 
