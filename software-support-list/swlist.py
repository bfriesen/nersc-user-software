#!/usr/bin/env python3
""" NERSC Software Support List Utility """

# processes to make sure work:
# - what software do I support?
# - who supports software xyz?
# - update record for xyz
# - get help about a field
# - get the list of available fields


import typing as t

# -----------
# utility: generate a short, fairly-unique string from the build and system fields,
# as a more user-friendly easier way to query for a record than typing out the 
# full build spec exactly
# code adapted from https://rosettacode.org/wiki/Base58Check_encoding#Python
_b58_alphabet = '123456789ABCDEFGHJKLMNPQRSTUVWXYZabcdefghijkmnopqrstuvwxyz'
def by58(i:int):
    while i>0:
        r,i = i%58,i//58
        yield r

def base58(i:int):
    return ''.join(_b58_alphabet[r] for r in by58(i))[::-1]

import xxhash
def get_hash(s:str) -> str:
    return base58(xxhash.xxh32(s).intdigest())

# ----------

from collections import abc, namedtuple
import json
import fastjsonschema
import re
import pendulum
import dateutil
from copy import deepcopy
import sys

JsonDict = t.Dict[str,'JsonType']
JsonType = t.Union[str,t.List['JsonType'],JsonDict]

class Schema(abc.Mapping):
    """ Schema presents a jsonschema as a dict-like collection of 
        field:field_metadata where the metadata are the details specified
        in the jsonschema and the fields are the fields of the json data 
        the schema describes. Nested fields are handled by dot-delimiting
        the levels, so eg if my_schema['current_support.level']['type']=='string'
        then my_json_data['current_support']['level']='Minimal' is 
        schema-compliant
        
        Properties of the schema itself (as opposed to of the json data
        it describes) can be accessed via get_meta, eg 
        my_schema.get_meta('definitions')

        Because flattening nested fields conflates arrays with their items and 
        objects with their definition, extra $specific_type metadata is added 
        to describe the type without potentially colliding with field names.
    """
    def __init__(self, schemafile:t.TextIO):
        self._meta = json.load(schemafile)
        self.assert_valid = fastjsonschema.compile(self._meta)
        # keep special track of definitions to support transparently dereferencing them:
        self._definitions = self._meta['definitions'] if 'definitions' in self._meta else {}

    def __repr__(self):
        return repr(self._meta)

    def get_meta(self, key, default=None):
        """ accessor for metadata (eg type, examples, order_hint) of the schema itself. 
            Having this accessor allows us to use __getitem__ as an accessor for fields 
            (in jsonschema, "properties") defined in the jsonschema
        """
        if default is None:
            return self._meta[key]
        else:
            return self._meta.get(key, default)

    _defn = re.compile(r'#/definitions/(\S+)')
    def _dereference(self,json_value:JsonType) -> t.Tuple[JsonDict,str]:
        " if d is a reference to a definition, descend into the defintion "
        if '$ref' in json_value:
            if m:=self._defn.match(json_value['$ref']):
                specific_type = m.group(1)
                return self._definitions[specific_type], specific_type
            else:
                raise NotImplementedError(f"can't handle reference with form {json_value['$ref']}")
        else:
            return json_value, json_value['type']  

    def __getitem__(self, item) -> JsonDict:
        """ schema[thing] returns the details ($type, format, etc) of the 
            property called "thing" 
        """
        non_schema_property = {"type":"string"} # unknown things are a string
        fields = self.get_meta('properties')
        # we will build up a copy, in order to add metadata without modifying the schema
        result = {} 
        for level in item.split('.'):
            describe_specific_type = lambda s: s
            details, specific_type = self._dereference(fields.get(level, non_schema_property))
            result = {'$specific_type': specific_type}
            result.update(details)
            if details['type'] == "array":
                details, item_type = self._dereference(details['items']) 
                describe_specific_type = lambda s: f"array[{item_type}]"
                result.update(details)
                result['type'] = "array"
                result['items'] = details
                result['items']['$specific_type'] = item_type
            if details['type'] == "object":
                fields = details['properties']
            if 'format' in details:
                specific_type = details['format']
            elif 'enum' in details:
                specific_type = "enum"
            result['$specific_type'] = describe_specific_type(specific_type)
        return result

    def __iter__(self):
        """ iterates over keys in the mapping, which for our purposes are the 
            fields (properties) defined in the schema. Follows the order hint 
            if there is one.
        """
        remaining_fields = set(self._meta['properties'])
        for p in self.get_meta('order_hint', []):
            remaining_fields.remove(p)
            yield p
        for p in self._meta['properties']:
            if p in remaining_fields:
                yield p 

    def __len__(self):
        return len(self._meta['properties'])

    def make_value_schema_compliant(self, path:str, value:str) -> JsonType:
        """ given a (possibly nested) field, and string value for that field,
            return a schema-compliant representation of it, or raise an exception
            if that is not possible
        """
        field = self.get(path)
        # next bit is only relevant for schema fields (others are just strings)
        if field:
            # convert to appropriate type/format/etc
            if field['type'] == "string":
                fmt = self[path].get('format',"")
                if fmt == "date-time":
                    value = pendulum.parse(value, tz='local').isoformat()
                if fmt == "uri":
                    # TODO not sure if/how to best enforce this
                    pass
                if "enum" in self[path]:
                    if value == "" and 'default' in field:
                        value = field['default']
                    # canonicalise case of enums then make sure it's a valid choice:
                    relevant_enums = {s.lower():s for s in self[path]["enum"]}
                    try:
                        value = relevant_enums[value.lower()]
                    except KeyError:
                        examples = '\n'.join(self[path]['examples'])
                        raise ValueError(f"{value} must be one of {examples}")
            elif field['type'] == "array":
                # convert a comma-separated list into an array:
                # TODO should probably gracefully handle eg quoting for list items tha contain a comma
                return value.split(',')
            else:
                raise TypeError(f"don't know how to turn {path} into a {field['type']}") 
        return value

    def _path_components(self, dot_delimited_name):
        """ given a property name like "current_support.datetime", this method returns
            a list of the parent,field,path-so-far for each component in the name, eg
            "past_support.datetime" results in
            [("","past_support","past_support"),("past_support","datetime","past_support.datetime")]
        """ 
        parts = dot_delimited_name.split('.')
        return [('.'.join(parts[:i]),part,'.'.join(parts[:i+1])) for i,part in enumerate(parts)]

    def fold(self, flat_dict:t.Dict[str,str]) -> JsonDict:
        """ given a flattened dict of data conforming to this schema, with 
            nesting indicated by dot-delimiting of field names, return a 
            schema-compliant equivalent nested dict
        """
        nested_dict = {}
        for col,val in flat_dict.items():
            # might need to descend into structure:
            d = nested_dict
            steps = self._path_components(col)
            for parent,field,path in steps[1:]:
                # FIXME if it should be an objectarray, make sure it is wrapped in an array
                d = d.setdefault(parent,{})
            parent,field,path = steps[-1]
            d[field] = self.make_value_schema_compliant(path, val)
        return nested_dict

    def __contains__(self, field):
        details = self._meta
        for level in field.split('.'):
            fields = details['properties']
            if level in fields:
                details, _ = self._dereference(fields.get(level))
            else:
                return False
        return True

    def flatten(self, nested_dict):
        """ given a nested dict of data conforming to this schema, flatten it 
            by dot-delimiting field names
        """
        #TODO
        raise NotImplementedError

    def get_flattened_headings(self, only:t.List[str]=None) -> t.List[str]:
        """ produce a list of headings corresponding to schema fields, 
            optionally a selected subset (sub-fields of fields in only
            are also returned)
        """
        source = self if only is None else only
        result = []
        for p in source:
            tp = self[p]['type']
            tp = self[p]['items']['type'] if tp == "array" else tp
            if tp == "object":
                # this only handles a 2-level schema, but that is what we have
                result += [f"{p}.{f}" for f in self[p]['properties']] 
            else:
                result.append(p)
        return result
                
    def get_folded_headings(self, only=None) -> t.List:
        raise NotImplementedError

    # TODO getting templates (flat/folded dict of headings with help or stub text) might be more useful

import functools
OR = lambda a,b: (a)|(b)

class WatermarkedDict(dict):
    """ dict that sets a "changed" flag if its contents change, which can be reset 
        calling watermark()
    """ 
    def __init__(self, *args, **kwargs):
        super(WatermarkedDict,self).__init__(*args, **kwargs)
        self.watermark()

    def watermark(self):
        self.changed = False

    def is_changed(self):
        self.changed |= functools.reduce(OR, (v.is_changed() for v in self.values() if isinstance(v,WatermarkedDict))) 
        return self.changed
        
    def __setitem__(self, key, value):
        self.changed |= self.get(key) != value
        super(WatermarkedDict,self).__setitem__(key, value)

    def __delitem__(self, key):
        self.changed = true
        super(WatermarkedDict,self).__delitem__(key)

    def update(self, otherdict) -> bool:
        for k in otherdict.keys() - self.keys():
            self.changed |= True
            break
        else:
            for k in self.keys() & otherdict.keys():
                self.changed |= self[k]!=otherdict[k]
        super(WatermarkedDict,self).update(otherdict)

    def copy(self) -> 'WatermarkedDict':
        return type(self)(self) 

    def pop(self, key, default=None):
        self.changed = True
        return super(WatermarkedDict,self).pop(key, default)

    def setdefault(self, key, value):
        self.changed |= self.get(key) != value
        return super(WatermarkedDict,self).setdefault(key, value)


class InsufficientNotice(Exception):
    def __init__(self, too_soon_date, acceptable_date):
        self.too_soon_date = too_soon_date
        self.acceptable_date = acceptable_date
        self.message = f"{too_soon_date} is too soon, {acceptable_date} is acceptable"

class IncorrectUsage(Exception):
    """ for don't call this method here errors """
    pass

class MissingJustification(Exception):
    def __init__(self, source):
        self.message = f"This update reduces support without a justification:\n{source}"

from fastjsonschema.exceptions import JsonSchemaException

# A software list Entry is fundamentally a 2-level json dictionary:
# some of it's properties are object or arrays or objects, but those objects 
# only contain strings
EntryDict = t.Dict[str,str]

#import traceback
#import sys
class Entry(abc.Mapping):
    """ Nested mapping representing the support entry for a single item of software,
        conforming to the schema
    """

    def __init__(self, schema:Schema, data:JsonDict=None):
        self.schema = schema
        self._data = WatermarkedDict(data) if data else None

    def __repr__(self):
        keys = [k for k in self.schema.get_meta('order_hint') if k in self._data]
        keys += [k for k in self._data if k not in keys]
        s = ', '.join([f"{repr(k)}: {repr(self._data[k])}" for k in keys])
        return f"Entry({{{s}}})"

    def assert_valid(self) -> None:
        try:
            self.schema.assert_valid(self._data)
        except JsonSchemaException as e:
            raise JsonSchemaException(f"Entry for {self}: {e.message}")
        return self

    def is_valid(self) -> bool:
        try:
            self.assert_valid()
        except JsonSchemaException as e:
            return False
        else:
            return True

    def key(self) -> t.Dict[str,str]:
        return {f: self[f] for f in self.schema.get_meta('primary_key')}

    def update_hash(self):
        """ use a simple function to put a short, repeatable-and-probably-unique 
            string based on the 'build' field, into the 'hash' field, to make 
            referring to a specific entry easier
        """
        self._data['hash'] = get_hash(self['build'])

    def compose(self, flat_dict:t.Dict[str,str], template:t.Optional['Entry']=None) -> 'Entry':
        """ compose an Entry from a flat_dict, optionally using a provided 'template'
            Entry as a starting point
        """
        if self._data is None:
            if template is None:
                self._data = WatermarkedDict()
            else:
                self._data = deepcopy(template._data)
        else:
            raise IncorrectUsage("don't re-compose an existing Entry!")

        nested_dict = self.schema.fold(flat_dict)
        # FIXME this feels like it is in the wrong place.
        # when ingesting new data, empty cells/fields should be interpreted to mean 
        # "no value for this field", not "this field should have an empty string". Handle
        # that here. though I suspet it really belongs elsewhere:
        # (prob with doing it earlier is, enums with a default get that set via fold())
        self._merge({k:v for k,v in nested_dict.items() if v!="" or k in self.schema.get_meta('primary_key')})
        self.update_hash()

        return self

    def update(self, other:'Entry', force=False) -> 'Entry':

        self._data.watermark()

        # check for policy compliance:
        # values and lambdas to help readability:
        now = pendulum.now().replace(microsecond=0)
        in_6_months = now + dateutil.relativedelta.relativedelta(now, months=6)
        when = lambda e: dateutil.parser.parse(e['planned_support.datetime'])
        level = lambda w,e: SupportLevel[e[f'{w}_support.level']]
        soonest_ok = min(when(self),in_6_months) if 'planned_support' in self else in_6_months

        def handle_violation(ex):
            if force:
                # record the violation in the status history
                self._data.setdefault('status_history', []).append(deepcopy(self['current_status']))
                now = pendulum.now().replace(microsecond=0).isoformat()
                self['status_history'].append({'datetime':now, 'status':'PolicyViolation'})
                self._data['current_status']['datetime'] = now
            else:
                raise ex

        #if not force:
        #  1. planned support reductions need 6 months notice and a justification
        if 'planned_support' in other:
            if 'planned_support' in self and level('planned',self) <= level('planned',other):
                if when(other) < soonest_ok:
                    handle_violation(InsufficientNotice(when(other), soonest_ok))
            else:
                if when(other) < in_6_months:
                    handle_violation(InsufficientNotice(when(other), in_6_months))
                if 'planned_support.justification' not in other:
                    raise MissingJustification(other)

        # 2. no unplanned support reductions, except to Restricted
        if ('current_support' in other and 
            level('current',other) < level('current',self) and
            other['current_support.level'] != 'Restricted'):
            if 'planned_support' not in self or when(self) > now:
                handle_violation(InsufficientNotice(now, soonest_ok))

        # any changes to current_support or current_status need to be recorded
        # in the relevant history field before update is applied:
        for k,history in (('current_support','past_support'), ('current_status','status_history')):
            if k in other and other[k] != self[k]:
                self._data.setdefault(history, []).append(deepcopy(self[k]))
                # the update might include a datetime, but if it does not we should use now:
                self._data[k]['datetime'] = pendulum.now().replace(microsecond=0).isoformat()
                # (actual update happens via _merge below)

        # if the update is enacting a planned support change, move the (pre-update) plan
        # into current_support in preparation for 
        if ('planned_support' in self and 'current_support' in other and
            other['current_support.level'] == self['planned_support.level']):    
                self._data['current_support'].update(self._data.pop('planned_support'))
        
        self._merge(other)
        
        if self._data.changed:
            updated = other.get('updated', pendulum.now().replace(microsecond=0).isoformat())
            self._data['updated'] = updated
            self.update_hash()

        return self

    def is_policy_compliant(self):
        raise NotImplementedError

    def _merge(self, nested_dict:EntryDict):
        """ pull a nested dict into self._data, with consideration for array,
            object and object-array fields in the schema
        """
        for k,v in nested_dict.items():
            if k in self.schema:
                if self.schema[k]['type'] == "array":
                    a = self._data.setdefault(k, [])
                    if self.schema[k]['items']['type'] == "object":
                        a.append(nested_dict[k])
                elif self.schema[k]['type'] == "object":
                    d = self._data.setdefault(k, {})
                    d.update(nested_dict[k])
                else: # string
                    self._data[k] = nested_dict[k]
            else:
                # non-schema field, must be a string
                self._data[k] = nested_dict[k]

    def __getitem__(self, item):
        d = self._data
        if '.' in item:
            # try to navigate to the nested item being referred to:
            for p in item.split('.')[:-1]:
                d = d[p]
        return d[item.rpartition('.')[-1]]
    
    def __iter__(self):
        return iter(self._data)

    def __len__(self):
        return len(self._data)

    def keys(self):
        return self._data.keys()

    def pop(self, key, default=None):
        return self._data.pop(key, default)

# ----------
# functions and data for the swlist itself:

import tinydb as tdb
import functools

# need to deal with dot-delimited stuff:
def where(k):
    parts = k.split('.')
    result = tdb.where(parts[0])
    for p in parts[1:]:
        result = result[p]
    return result

AND = lambda a,b: (a)&(b)
def exact_query(**terms):
    return functools.reduce(AND, [(where(k)==v) for k,v in terms.items()])
    #return functools.reduce(AND, [(tdb.where(k)==v) for k,v in terms.items()])

def regex_query(**terms):
    return functools.reduce(AND, [where(k).search(v) for k,v in terms.items()])
    #return functools.reduce(AND, [tdb.where(k).search(v) for k,v in terms.items()])

class SWList:
    """ state for the actual swlist """
    def __init__(self, filename:str, path_to_schema:str = 'etc/schema.json'):
        self.db = tdb.TinyDB(filename, sort_keys=True, indent=4, separators=(',', ': '))
        with open(path_to_schema) as schemafile:
            self.schema = Schema(schemafile)

    def __repr__(self):
        return f"SWList iwth {len(self.db)} entries: {[e['name'] for e in self.db]}"

    def __len__(self):
        return len(self.db)

    def find(self, name, **build_field):
        """ find the entry uniquely identified by the name and either a build
            string or (optional, for convenience) hash
        """
        if "build" not in build_field and "hash" not in build_field:
            raise TypeError("expected at least one of 'build' or 'hash' arguments")
        result = self.db.search(exact_query(name=name, **build_field))
        if len(result) != 1:
            raise ValueError("arguments did not uniquely identify an entry")
        return Entry(self.schema, result[0])

    def collect_exact_matches(self, **search_terms) ->t.Iterator[Entry]:
        for r in self.db.search(exact_query(**search_terms)):
            yield Entry(self.schema, r)

    def collect_regex_matches(self, **search_terms) ->t.Iterator[Entry]:
        # TODO exact vs regex match
        for r in self.db.search(regex_query(**search_terms)):
            yield Entry(self.schema, r)

    def upsert(self, entry:Entry):
        self.db.upsert(entry, exact_query(**entry.key()))

    def is_valid(self) -> bool:
        return functools.reduce(lambda a,b: a and b, (e for e in self.db))

    def __contains__(self, search_terms:t.Dict[str,str]):
        return self.db.contains(exact_query(**search_terms))
        

from enum import IntEnum
class SupportLevel(IntEnum):
    Restricted = 0
    Minimal = 1
    Provided = 2
    Priority = 3
    Deprecated = 4

import csv

def do_template(params):
    with open(params['schema']) as schema_file:
        schema = Schema(schema_file)

    if params['fmt'] in ('tsv','csv'):
        delimiter = '\t' if params['fmt']=='tsv' else ','
        headings = schema.get_flattened_headings(params['fields'])
        with open(params['outfile'], 'w', newline='') as outfile:
            writer = csv.DictWriter(outfile, delimiter=delimiter, fieldnames=headings)
            writer.writeheader()
#    elif params['fmt'] == 'json':
        
    else:
        raise NotImplementedError

def do_ingest(params):
    swlist = SWList(params['dbpath'], params['schema'])

    # FIXME: in some cases multiple entries in the current swlist have the 
    # same name and build (though other fields are different), catch, flag and skip
    # these
    # better still, in those cases pad the build field with an easily-searchable tag, 
    # then can correct thhose entries via this tool

    # keep track of unique and duplicated entries from <infile>
    entries_read = {}
    num_read = 0
    num_added = 0
    num_updated = 0
    num_duplicates = 0 

    if params['on_duplicate_pkey'] == 'skip':
        verb = 'skipped'
        def handle_duplicate(e: Entry, key:str):
            nonlocal num_duplicates
            num_duplicates += 1
    elif params['on_duplicate_pkey'] == 'flag':
        verb = 'added with DUPLICATE flag in build field'
        now = pendulum.now().replace(microsecond=0).isoformat()
        def handle_duplicate(e: Entry, key:str):
            nth = entries_read[key]
            entries_read[key] = nth+1
            build = f"{e['build']} DUPLICATE {nth} added {now}"
            e_new = Entry(swlist.schema).compose({'build':build}, template=e)
            nonlocal num_duplicates
            num_duplicates += 1
            swlist.upsert(e_new)
            nonlocal num_added
            num_added += 1
    elif params['on_duplicate_pkey'] == 'abort':
        def handle_duplicate(e: Entry, key:str):
            e2 = entries_read[key]
            raise ValueError(f"Duplicate entry ({e} resolves to same as {e2})")
    else:
        def handle_duplicate(e: Entry, key:str):
            e2 = entries_read[key]
            raise ValueError(f"Duplicate entry ({e} resolves to same as {e2}) and no strategy for handling it")
    
    if params['fmt'] in ('tsv','csv'):
        delimiter = '\t' if params['fmt']=='tsv' else ','
        with open(params['infile'], 'r', newline='') as infile:
            reader = csv.DictReader(infile, delimiter=delimiter)
            headings = next(reader) 
            reader = csv.DictReader(infile, fieldnames=headings, delimiter=delimiter)
            for row in reader:
                num_read += 1
                e = Entry(swlist.schema).compose(row).assert_valid()
                if not any(e.key().values()):
                    sys.stderr.write(f"WARNING: skipping this entry with no name or build: {e}\n")
                    continue 
                key = ' '.join([f"{k}={v}" for k,v in e.key().items()])
                if key in entries_read:
                    handle_duplicate(e, key)
                else:
                    if e.key() in swlist:
                        num_updated += 1
                    else:
                        num_added += 1
                    swlist.upsert(e)
                entries_read[key] = 1
        print(f"ingesting {num_read} entries: {num_added} added, {num_duplicates} {verb}, {num_updated} updated") 
    else:
        raise NotImplementedError

# borrowed from https://stackoverflow.com/questions/17602878/how-to-handle-both-with-open-and-sys-stdout-nicely/17603000
import sys
import contextlib

@contextlib.contextmanager
def outhandle(filename=None, *args, **kwargs):
    if filename and filename != '-':
        fh = open(filename, 'w', *args, **kwargs)
    else:
        fh = sys.stdout

    try:
        yield fh
    finally:
        if fh is not sys.stdout:
            fh.close()

def do_find(params):
    swlist = SWList(params['dbpath'], params['schema'])
    terms = {}
    for i,clause in enumerate(params['terms']):
        f,sep,v = clause.partition("=")
        if not sep and i==0:
            # support "find petsc" as shorthand for "find name=petsc":
            f,v = 'name',clause
        if not sep and clause[0]=='/':
            # support "find .. /e4Tas .." as shorthand for "find .. hash=e4Tas ..":
            f,v = 'hash',clause[1:]
        if not f in swlist.schema:
            # support common shorthands like owner for current_support.owner:
            if f"current_support.{f}" in swlist.schema:
                #print(f"using current_support.{f}")
                f = f"current_support.{f}"
            elif f"current_status.{f}" in swlist.schema:
                #print(f"using current_status.{f}")
                f = f"current_status.{f}"
            else:
                sys.stderr.write(f"WARNING: {f} is not a field in {params['schema']}\n") 
        terms[f] = v

    if params['regex']:
        finder = swlist.collect_regex_matches(**terms)
    else:
        finder = swlist.collect_exact_matches(**terms)

    if params['fmt'] in ('tsv','csv'):
        delimiter = '\t' if params['fmt']=='tsv' else ','
        headings = swlist.schema.get_flattened_headings(params['fields'])
        with outhandle(params.get('outfile'), newline='') as outfile: 
            writer = csv.DictWriter(outfile, delimiter=delimiter, fieldnames=headings, extrasaction='ignore')
            writer.writeheader()
            for e in finder:
                writer.writerow(e)


def do_validate(params):
    raise NotImplementedError

def do_fields(params):
    raise NotImplementedError


import argparse
import sys
if __name__ == '__main__':

    description = "Utilities to query and update the NERSC software support list"
    usage       = "%(prog)s <common-options> <command> <command-args>"

    command_help = {
        # command     argument    effect (help)
        'ingest':   ('<infile>',  "incorporate entries/updates from <infile> into the list"),
        'template': ('<outfile>', "produce a template (eg csv with header line only) " +
                                  "that you can populate and use as input to ingest"),
        'find':     ('<search-terms>', "return entries matching the requested <search-terms>. " +
                                   "<search-terms> are like field=value. You can omit the " + 
                                   "field in the first term for 'name' and in the second for " + 
                                   "'build', or denote a build hash with '/' eg '/abCd'"),
        'validate': ('[<file>]',  "check that a file (or the list) conforms to the schema"),
        'fields':   (None,        "list the schema-defined fields and what they should contain"),
    }

    common_options = {
        # long      short   default                    meaning (help)
        '--dbpath': ('-d', "data/software-list.json", "path to the tinydb-compatible json " +
                                                      "file of the software support list"),
        '--schema': ('-s', "etc/schema.json",         "path to the jsonschema file used to " +
                                                      "validate updates to the list"),
        '--fmt':    ('-f', "tsv",                     "format of <infile> and/or <outfile>. " +
                                                      "Can be tsv, csv, json or yaml"),
        '--fields': ('-F', ":short",                  "limit output to specified fields. " +
                                                      "Some predefined sets of fields can " +
                                                      "be specified via a :foo convention, " +
                                                      "eg -F :short or -F :short,owner"),
    }

    # predefined field sets:
    fieldsets = {
        ":short":    "name,hash,build,system,access_advice,short_help,current_support.level,current_status.status",
        ":access":   "name,build,system,access_advice,access_url,installed_at",
        ":help":     "name,owner,description,software_type,short_help,help_url,known_issues,notes",
        ":build":    "name,owner,build,build_artifacts,tests,notes",
        ":required": "updated,name,build,current_support,current_status",
        ":support":  "updated,name,hash,build,current_support.level,current_support.owner,current_status"
    }
    

    parser = argparse.ArgumentParser(description=description, usage=usage)

    # commmon options:
    SHORT, DEFAULT, HELP = 0,1,2
    for name,arg in common_options.items():
        parser.add_argument(name, arg[SHORT], help=arg[HELP], default=arg[DEFAULT])

    # commands
    subparsers = parser.add_subparsers(dest='command', title="commands")
    ARG, HELP = 0,1

    cmd = 'template'
    p = subparsers.add_parser(cmd, help=command_help[cmd][HELP])
    p.add_argument('outfile', help="path to output file")
    p.set_defaults(func=do_template)

    cmd = 'ingest'
    p = subparsers.add_parser(cmd, help=command_help[cmd][HELP])
    p.add_argument('infile', help="path to file to ingest")
    dup_help = "strategy for seemingly-duplicate entries in <infile>\n"
    dup_help += "(list requires name + build to be unique per entry)"
    p.add_argument('--on-duplicate-pkey', choices=['skip','flag','abort'], 
        default='flag', help=dup_help)
    p.set_defaults(func=do_ingest)

    cmd = 'find'
    p = subparsers.add_parser(cmd, help=command_help[cmd][HELP])
    p.add_argument('--regex','-r', help="use regex in search terms eg name=^petsc",
                   action='store_true')
    p.add_argument('--outfile', '-o', help="write results to <outfile> (default stdout)")
    p.add_argument('terms', help="search terms eg name=foo owner=elvis", nargs='+')
    p.set_defaults(func=do_find)

    cmd = 'validate'
    p = subparsers.add_parser(cmd, help=command_help[cmd][HELP])
    p.add_argument('file', help="optional file to validate")
    p.set_defaults(func=do_validate)

    cmd = 'fields'
    p = subparsers.add_parser(cmd, help=command_help[cmd][HELP])
    p.set_defaults(func=do_fields)

    args = sys.argv[1:]
    if len(args) < 1:
        parser.print_help()
        parser.exit()

    params = vars(parser.parse_args(args)).copy()
    #print(params)

    # handle some common options:
    expanded_field_list = []
    for word in params['fields'].split(','):
        if word == ':all':
            params['fields'] = None
            break
        as_list = [f for f in fieldsets[word].split(',')] if word in fieldsets else [word]
        for f in as_list:
            if f not in expanded_field_list:
                expanded_field_list.append(f)
    else:
        params['fields'] = expanded_field_list

    # do the requested action:
    params['func'](params)

    


#old_usage = """
#
#Usage
#-----
#::
#
#    ./swlist.py [help|-h|--help]
#        Print this usage and exit.
#
#    ./swlist.py [-v] [-f fmt] fields
#        Print the list of schema fields and the comment describing each.
#
#        With ``-v`` additional info eg enums and examples are printed.
#        With ``-f fmt`` the output is in the requested format (eg yaml or tsv).
#
#    ./swlist.py [-d dbpath] [-f fmt] [-F fields] current [search-terms]
#    ./swlist.py [-d dbpath] [-f fmt] [-F fields] now [search-terms]
#        Print the current (and planned) support information for software
#        matching any provided search-terms (by default: all).
#
#        With ``-d dbpath`` the json file at dpbath will be read (default swlist.json). 
#        With ``-f fmt`` the output is in the requested format (eg yaml or tsv).
#
#        With ``-F fields`` where fields is a comma-separated list of schema fields,
#        only those fields will be shown. Field names beginning with a colon are 
#        expanded to the following groups:
#            :short   name,build,system,level,status
#            :access  name,build,system,access_advice,access_url,installed_at
#            :help    name,owner,description,software_type,short_help,help_url,known_issues,notes 
#            :build   name,owner,build,build_artifacts,tests,notes
#        Already-included fields in groups are not repeated
#
#        Search terms starting with ``fieldname:`` limit results to records 
#        containing that substring in that field. Otherwise, the name,
#        build, hash and description fields are searched.
#
#    ./swlist.py [-d dbpath] [-f fmt] history <name> [filters]
#        Print the support history for a specific software
#
#    ./swlist.py [-d dbpath] [-f fmt] update <file>
#        Update the software list according to the records in <file> 
#
#"""
