Feature: Producing a template for info-to-be-ingested

    As a consultant
    I want to create an empty csv or json or whatever file with field headings
    So that I can fill it in when creating a new entry

    Scenario: Flattened subset of top-level headings for ingesting from a table
    # tsv or csv, the point is it is a flattened table
    Given a format selection of "tsv"
    And a field list of "name,build,current_support,current_status"
    When headings are requested
    Then a list should be produced matching
        | item                          |
        | name                          |
        | build                         |
        | current_support.datetime      |
        | current_support.level         |
        | current_support.justification |
        | current_support.owner         |
        | current_support.notes         |
        | current_status.datetime       |
        | current_status.status         |
        | current_status.known_issues   |

    Scenario: Flattened subset of specific headings for ingesting from a table
    # tsv or csv, the point is it is a flattened table
    Given a format selection of "tsv"
    And a field list of "name,build,current_support.owner"
    When headings are requested
    Then a list should be produced matching
        | item                          |
        | name                          |
        | build                         |
        | current_support.owner         |

    Scenario: Flattened subset of specific headings and non-schema fields for ingesting from a table
    # tsv or csv, the point is it is a flattened table
    Given a format selection of "tsv"
    And a field list of "name,build,special_info,current_status,current_status.reported_by"
    When headings are requested
    Then a list should be produced matching
        | item                          |
        | name                          |
        | build                         |
        | special_info                  |
        | current_status.datetime       |
        | current_status.status         |
        | current_status.known_issues   |
        | current_status.reported_by    |

    Scenario: Flattened full set of headings for ingesting from a table
    Given a format selection of "tsv"
    And no field list
    When headings are requested
    Then a list should be produced matching
        | item                          |
        | updated                       |
        | name                          |
        | hash                          |
        | build                         |
        | access_advice                 |
        | access_url                    |
        | installed_at                  |
        | build_artifacts               |
        | system                        |
        | software_type                 |
        | description                   |
        | short_help                    |
        | help_url                      |
        | current_support.datetime      |
        | current_support.level         |
        | current_support.justification |
        | current_support.owner         |
        | current_support.notes         |
        | current_status.datetime       |
        | current_status.status         |
        | current_status.known_issues   |
        | planned_support.datetime      |
        | planned_support.level         |
        | planned_support.justification |
        | planned_support.owner         |
        | planned_support.notes         |
        | past_support.datetime         |
        | past_support.level            |
        | past_support.justification    |
        | past_support.owner            |
        | past_support.notes            |
        | status_history.datetime       |
        | status_history.status         |
        | status_history.known_issues   |
        | tests                         |
        | executables                   |
        | dependencies.name             |
        | dependencies.build            |
        | notes                         |

#    Scenario: Structured headings for ingesting from json or yaml
#    Given a format selection of "json"
#    And a field list of "name,build,current_support.owner"
#    When headings are requested
    

#    Scenario: Schema-defined enums with examples

#    
