Feature: Querying the list

    This is where most people will use the list, most of the time.
    Identified roles include: software team, software owner, consultant, user.

    As a user or consultant
    I want to see relevant slices of the software list
    So I know what support a package has now, or in the past or future

    As a software owner
    I want to see relevant slices of the software list
    So I know what support I need to prepare and provide

#    Background: Having a list to query
#        # The list should cover the range of scenarios, eg software with
#        # and without a planned change, software at different support levels,
#        # software with and without a history, multiple versions or entries 
#        # for whatever reason, etc
#
#        Given the sample list in "test/sample-list.json"
#        #And the software list can be used by tinydb
#        #And the software list conforms to the schema
#
#    Scenario: Getting the current support level for all listed software
#        When the user queries current support levels
#        Then the returned records should include
#            | name  | build                         | access_advice | current_support.level |
#            | petsc | @3.11.2 %intel target=haswell | module        | Provided              |
#
#    Scenario: finding out about a specific software
#    Scenario: getting the list of software I am responsible for

# a useful query will be "list outstanding changes" (eg planned support reductions, other things 
# that need action eg something poorly supported, or that had a support reduction with 
# insufficient notice, but sufficient time has now passed
