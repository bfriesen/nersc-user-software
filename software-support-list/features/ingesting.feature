Feature: Ingesting an entry

    As a consultant
    I want to make edits in some convenient format (eg csv or yaml) 
    To update the swlist

    Some UI function should handle parsing the input format and providing an 
    iterator over dict-like objects. The scenarios here describe what should
    happen for examples of entries

    A sufficiently-complete entry is one with all of the "required" fields
    specified by etc/schema.json, with the optional exception of "updated"
    which should then be set to the current time

    Scenario: Adding a sufficiently-complete entry to a new, empty list
        Given an empty software list
        And a valid entry for a Provided software with
            | field    | value       |
            | name     | matlab      |
            | build    | @MCR_R2016a |
            | updated  | 2020-05-14  |
        When the entry is upserted to the software list
        Then the software list contains 1 entries
        And the list should be valid

    Scenario: Adding a new, sufficiently-complete entry to an existing list 
        Given a software list with an entry for "matlab" "@MCR_R2016a"
        And the software list contains 1 entries
        And a valid entry for a Provided software with
            | field    | value            |
            | name     | petsc            |
            | build    | @3.11.2 +complex |
            | updated  | 2020-10-14       |
        When the entry is upserted to the software list
        Then the software list contains 2 entries
        And the list should be valid

    Scenario: Adding support notes to an existing entry
        Given a software list with an entry for "matlab" "@MCR_R2016a"
        And the entry has no short_help field
        # have an uninvolved field in the existing entry, to test that 
        # it remains unmolested:
        And the entry has a software_type field
        And a partial entry with short_help for that same software
        And the software list contains 1 entries
        When the partial entry is upserted to the software list
        Then the software list contains 1 entries
        And the entry after upsert should still contain the fields it had before upsert
        And the entry after upsert should contain the new short_help
        And the list should be valid

    Scenario: Non-controversial update to an existing entry
        Given a software list with an entry for "matlab" "@MCR_R2016a"
        And the entry has a description field
        # have an uninvolved field in the existing entry, to test that 
        # it remains unmolested:
        And the entry has a software_type field
        And a partial entry with description for that same software
        And the software list contains 1 entries
        When the partial entry is upserted to the software list
        Then the software list contains 1 entries
        And the entry after upsert should contain the updated description
        And the entry after upsert should be unchanged except for description
        And the list should be valid

## this might be a separate feature:
## another scenario / use-case is listing dependencies, the entry is empty except 
## for the depedency fields, and it is take that the most-recently-specified name 
## and build identify the software in question.
## The pair to this is some means to clear fields - maybe the value "None" should do this
##    Scenario: Ingesting an entry where most fields are blank, to add a dependency
##        Given a software list with an entry for "matlab" "@MCR_R2016a"
##        And a partial entry with many blank fields a new status record for that software 
#        
