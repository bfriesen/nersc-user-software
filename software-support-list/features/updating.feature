Feature: Updating an entry

    As a consultant
    I want my updates to the software list to be validated
    So that I can make changes without worrying too much about breaking things

    Background: Set up the entry we will modify
        # for these tests we only use the swlist itself for its schema, the 
        # updates are against the local sample entry
        Given an empty software list
        And a valid entry for a Provided software with
            | field    | value       |
            | name     | matlab      |
            | build    | @MCR_R2016a |
            | updated  | 2020-05-14  |

    Scenario: Increasing the support level of a software
        # an increase doesn't need advance notice, just move current 
        # support to past support and update current support
        Given a partial entry increasing the support level for that software
        When the partial entry is applied to the initial entry
        Then the resulting entry should have the new current_support.level 
        And the resulting entry should have the previous level in its past support
        And the resulting entry should be valid

    Scenario: Planning a support reduction with sufficient notice
        # basically the same as a non-controversial update
        Given a partial entry with a planned support reduction dated after 6 months
        When the partial entry is applied to the initial entry
        Then the resulting entry should have a planned support reduction dated after 6 months
        And the resulting entry should be valid

    Scenario: Attempting to plan a support reduction with insufficient notice
        Given a partial entry with a planned support reduction dated tomorrow
        When the partial entry is applied to the initial entry
        Then a InsufficientNotice exception should be raised
        And the entry should be unchanged

    Scenario: Attempting to plan a support reduction without a justification
        Given a partial entry with a planned support reduction with no justification
        When the partial entry is applied to the initial entry
        Then a MissingJustification exception should be raised
        And the entry should be unchanged

    Scenario: Changing a planned support reduction to a sooner date
        Given the entry has a planned support reduction dated next month
        And a partial entry with a planned support reduction dated tomorrow
        When the partial entry is applied to the initial entry
        Then a InsufficientNotice exception should be raised
        And the entry should be unchanged

    Scenario: Changing a planned support reduction to a later date
        Given the entry has a planned support reduction dated next month
        Given a partial entry with a planned support reduction dated after 2 months
        When the partial entry is applied to the initial entry
        Then the resulting entry should have a planned support reduction dated after 2 months
        And the resulting entry should be valid

    Scenario: Attempting an unplanned support reduction
        Given the entry has no planned_support field
        And a partial entry decreasing the support level for that software
        When the partial entry is applied to the initial entry
        Then a InsufficientNotice exception should be raised
        And the entry should be unchanged

    Scenario: Applying a planned support reduction
        Given the entry has a planned support reduction dated today
        And a partial entry decreasing the support level for that software
        When the partial entry is applied to the initial entry
        Then the resulting entry should have the new current_support.level 
        And the resulting entry should have the previous level in its past support
        And the resulting entry should be valid

    Scenario: Attempting a planned support reduction before scheduled
        Given the entry has a planned support reduction dated next month
        And a partial entry decreasing the support level for that software
        When the partial entry is applied to the initial entry
        Then a InsufficientNotice exception should be raised
        And the entry should be unchanged

    Scenario: Restricting a software
        # restricted is a special case, no notice is required
        Given a partial entry restricting use of that software
        When the partial entry is applied to the initial entry
        Then the resulting entry should have the new current_support.level 
        And the resulting entry should have the previous level in its past support
        And the resulting entry should be valid

    Scenario: Forcing a change past an InsufficientNotice exception
        Given a partial entry that will trigger an InsufficientNotice exception
        When the partial entry is forcefully applied to the initial entry
        Then a InsufficientNotice exception should be raised
        And the status history should include the prior status record
        And the status history should include the PolicyViolation status record
        And the resulting current_status.status should equal the prior current_status.status
        And the resulting entry should be valid

#    # hmm, we can add and change fields, but how to clear them?
#    # maybe explicitly set it to empty or None? But an empty cell is a cvs will 
#    # probably show as "" and thus be ambiguous. Could the text "None" be?
#    Scenario: Cancelling a planned support reduction
#        Given the entry has a planned support reduction dated next month
#        And a partial entry with  .. er, what?
#        When the partial entry is applied to the initial entry
#        Then the planned support should be cleared
#        And the resulting entry should be valid
#
    Scenario: Updating the current status of a software
        Given a partial entry with a status record for that software 
        # to consider: what if the new status record is the same as the old one? or
        # only updates the (optional) known_issues text?
        #And the partial entry status is different from the entry current status
        When the partial entry is applied to the initial entry
        Then the status history should include the prior status record
        And the resulting current_status should reflect the partial current_status
        And the resulting entry should be valid

