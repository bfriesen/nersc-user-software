#!/usr/bin/env python3

import os
import swlist
import example_data
import datetime
import dateutil
from copy import deepcopy
import traceback
from collections import abc

## === the actions we are testing ===

@when(u'the partial entry is applied to the initial entry')
def step_impl(context):
    context.entry_before_update = deepcopy(context.entry)
    try:
        print(f"before update: {context.entry_before_update}")
        print(f"partial entry: {context.partial_entry}")
        context.entry_after_update = context.entry.update(context.partial_entry)
        print("no exception!")
        print(context.entry_after_update)
        print(context.entry_before_update)
        print(context.entry)
    except Exception as ex:
        print(f"caught an exception {ex}")
        traceback.print_exc()
        context.exception = ex

@when(u'the partial entry is forcefully applied to the initial entry')
def step_impl(context):
    context.entry_before_update = deepcopy(context.entry)
    try:
        print(f"before update: {context.entry_before_update}")
        print(f"partial entry: {context.partial_entry}")
        context.entry_after_update = context.entry.update(context.partial_entry)
        print("no exception!")
        print(context.entry_after_update)
    except Exception as ex:
        context.exception = ex
        print("hit exception, trying again with force")
        context.entry_after_update = context.entry.update(context.partial_entry, force=True)
        print(f"after exception and retry: {context.entry_after_update}")

## ===

def _when_as_datetime(when:str) -> datetime.datetime:
    now = datetime.datetime.now()
    if when == "today":
        return now.replace(hour=0, minute=0, second=0, microsecond=0,tzinfo=None)
    if when == "yesterday":
        return now + datetime.timedelta(days=-1)
    elif when == "tomorrow":
        return now + datetime.timedelta(days=+1)
    elif when == "next month":
        return now + dateutil.relativedelta.relativedelta(now, months=1)
    elif when == "after 2 months":
        return now + dateutil.relativedelta.relativedelta(now, months=2, days=1)
    elif when == "after 6 months":
        return now + dateutil.relativedelta.relativedelta(now, months=6, days=1)
    else:
        raise NotImplementedError(f"test routine doesn't understand {when}")

def _assert_datetime_matches(t1, t2, tag=""):
    """ python datetime handling is a mess, this is a fuzzy compare to give some 
        confidence that things worked, without diving into the deranged rabbithole 
        of trying to get two datetimes to be actually equal
    """
    t1 = dateutil.parser.parse(t1).replace(minute=0, second=0, microsecond=0,tzinfo=None).isoformat()
    t2 = dateutil.parser.parse(t2).replace(minute=0, second=0, microsecond=0,tzinfo=None).isoformat()
    assert t1 == t2, f"{tag}: {t1}  != {t2}"
    
## ===

@given(u'a partial entry increasing the support level for that software')
def step_impl(context):
    # make sure we're testing what we think we are testing:
    assert context.entry['current_support']['level'] != 'Priority'

    # make a higher-support-level partial entry:
    partial_entry_dict = example_data.valid_priority_support_record_dict("current_support.")
    partial_entry_dict['name'] = context.entry['name']
    partial_entry_dict['build'] = context.entry['build']
    # hopefully people will actually put sensible values in datetime field:
    # for testing, work around pythons datetime mishandling:
    today = datetime.datetime.now().replace(hour=0, minute=0, second=0, microsecond=0,tzinfo=None)
    partial_entry_dict['current_support.datetime'] = today.isoformat()
    context.partial_entry = swlist.Entry(context.swlist.schema).compose(partial_entry_dict)

@given(u'a partial entry decreasing the support level for that software')
def step_impl(context):
    # make sure we're testing what we think we are testing:
    assert context.entry['current_support']['level'] != 'Minimal'

    # make a lower-support-level partial entry:
    partial_entry_dict = example_data.valid_minimal_support_record_dict("current_support.")
    partial_entry_dict['name'] = context.entry['name']
    partial_entry_dict['build'] = context.entry['build']
    # hopefully people will actually put sensible values in datetime field:
    # for testing, work around pythons datetime mishandling:
    today = datetime.datetime.now().replace(hour=0, minute=0, second=0, microsecond=0,tzinfo=None)
    partial_entry_dict['current_support.datetime'] = today.isoformat()
    context.partial_entry = swlist.Entry(context.swlist.schema).compose(partial_entry_dict)
    print(context.entry)

@given(u'a partial entry with a planned support reduction dated {when}')
def step_impl(context, when:str):
    # minimal support is a support reduction:
    partial_entry_dict = example_data.partial_provided_entry_dict(context.entry['name'],context.entry['build'])
    w = _when_as_datetime(when).isoformat()
    partial_entry_dict.update(example_data.valid_minimal_support_record_dict(prefix='planned_support.',when=w))
    context.partial_entry = swlist.Entry(context.swlist.schema).compose(partial_entry_dict)

@given(u'a partial entry with a planned support reduction with no justification')
def step_impl(context):
    # minimal support is a support reduction:
    partial_entry_dict = example_data.partial_provided_entry_dict(context.entry['name'],context.entry['build'])
    w = _when_as_datetime('after 6 months').isoformat()
    partial_entry_dict.update(example_data.valid_minimal_support_record_dict(prefix='planned_support.',when=w))
    partial_entry_dict.pop('planned_support.justification')
    context.partial_entry = swlist.Entry(context.swlist.schema).compose(partial_entry_dict)

@given(u'a partial entry that will trigger an InsufficientNotice exception')
def step_impl(context):
    assert 'planned_support' not in context.entry
    assert context.entry['current_support']['level'] != 'Minimal'
    # make a lower-support-level partial entry:
    partial_entry_dict = example_data.valid_minimal_support_record_dict("current_support.")
    partial_entry_dict['name'] = context.entry['name']
    partial_entry_dict['build'] = context.entry['build']
    # hopefully people will actually put sensible values in datetime field:
    # for testing, work around pythons datetime mishandling:
    today = datetime.datetime.now().replace(hour=0, minute=0, second=0, microsecond=0,tzinfo=None)
    partial_entry_dict['current_support.datetime'] = today.isoformat()
    context.partial_entry = swlist.Entry(context.swlist.schema).compose(partial_entry_dict)

@given(u'the entry has a planned support reduction dated {when}')
def step_impl(context, when:str):
    planned_reduction = example_data.partial_provided_entry_dict(context.entry['name'],context.entry['build'])
    w = _when_as_datetime(when).isoformat()
    planned_reduction.update({'planned_support.level':'Minimal','planned_support.datetime':w})
    context.entry = swlist.Entry(context.swlist.schema).compose(planned_reduction, context.entry)
    print(context.entry)

@given(u'a partial entry restricting use of that software')
def step_impl(context):
    # make a "restricted"  partial entry:
    now = datetime.datetime.now().replace(tzinfo=None).isoformat()
    partial_entry_dict = example_data.valid_minimal_support_record_dict("current_support.", when=now)
    partial_entry_dict['current_support.level'] = 'Restricted'
    partial_entry_dict['name'] = context.entry['name']
    partial_entry_dict['build'] = context.entry['build']
    context.partial_entry = swlist.Entry(context.swlist.schema).compose(partial_entry_dict)

@given(u'the partial entry has no {field} field')
def step_impl(context, field:str):
    context.partial_entry.pop(field)
    print(context.partial_entry)
    assert field not in context.partial_entry

@given(u'a partial entry with a status record for that software')
def step_impl(context):
    new_status = example_data.partial_provided_entry_dict(context.entry['name'],context.entry['build'])
    now = datetime.datetime.now().isoformat()
    status_dict = example_data.raw_valid_status_record_dict(prefix='current_status.', datetime=now)
    # make it more obvious that the status has changed:
    status_dict['current_status.status'] = 'Working'
    new_status.update(status_dict)
    context.partial_entry = swlist.Entry(context.swlist.schema).compose(new_status)

## ==

@then(u'the resulting entry should have the new {field}')
def step_impl(context, field):
    print(f"\npartial_entry: {context.partial_entry}\nentry_after_update: {context.entry_after_update}")
    assert context.entry_after_update[field] == context.partial_entry[field], \
        f"\npartial_entry: {context.partial_entry}\nentry_after_update: {context.entry_after_update}"

@then(u'the resulting entry should have the previous level in its past support')
def step_impl(context):
    history = context.entry_after_update['past_support']
    quarry = context.entry_before_update['current_support']
    for i in history:
        if i['datetime'] == quarry['datetime']:
            assert i == quarry
            break
    else:
        raise KeyError("no matching entry found in history")

@then(u'the resulting entry should be valid')
def step_impl(context):
    context.entry_after_update.assert_valid()

@then(u'the resulting entry should have a planned support reduction dated {when}')
def step_impl(context, when:str):
    prior_support = swlist.SupportLevel[context.entry_before_update['current_support.level']]
    new_support = swlist.SupportLevel[context.entry_after_update['planned_support.level']]
    assert new_support < prior_support 
    w = _when_as_datetime(when).isoformat()
    _assert_datetime_matches(context.entry_after_update['planned_support.datetime'], w)

@then(u'a {exception_type} exception should be raised')
def step_impl(context, exception_type: str):
    assert context.exception.__class__.__name__ == exception_type

@then(u'the entry should be unchanged')
def step_impl(context):
    assert context.entry == context.entry_before_update

@then(u'the status history should include the {what} status record')
def step_impl(context, what:str):
    if what == 'prior': 
        # looking for whatever the prior status was
        quarry = context.entry_before_update['current_status.status']
        when = context.entry_before_update['current_status.datetime']
    else:
        # looking for a specific status, eg "Working" or "PolicyViolation"
        quarry = what
        when = context.entry_after_update['current_status.datetime']
    history = context.entry_after_update['status_history']
    for i in history:
        print(f"looking for {quarry} at {when} in {history}")
        if i['datetime'] == when and i['status'] == quarry:
            break
    else:
        raise KeyError(f"no matching entry found in history")

@then(u'the resulting {field1} should equal the {which} {field2}')
def step_impl(context, field1:str, which:str, field2:str):
    resulting = context.entry_after_update[field1]
    if which=='prior':
        target = context.entry_before_update[field2]
    elif which == 'partial':
        target = context.partial_entry[field2]
    assert resulting == target, f"{resulting} != {target}"

@then(u'the resulting {field1} should reflect the {which} {field2}')
def step_impl(context, field1:str, which:str, field2:str):
    " relect meaning include-updates-from, not necessarily equal "
    resulting = context.entry_after_update[field1]
    if which=='prior':
        target = context.entry_before_update[field2]
    elif which == 'partial':
        target = context.partial_entry[field2]
    assert isinstance(resulting, abc.Mapping), f"Error reflect is for dict types, not {resulting}"
    assert isinstance(resulting, abc.Mapping), f"Error reflect is for dict types, not {target}"
    for k,v in target.items():
        assert resulting[k] == v, f"mismatch for {k} between {target}, {resulting}"





