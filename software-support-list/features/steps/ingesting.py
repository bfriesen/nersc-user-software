#!/usr/bin/env python3

import os
import swlist
import example_data
import datetime
import dateutil

## === the actions we are testing ===

@when(u'the entry is upserted to the software list')
def step_impl(context):
    name = context.entry['name']
    build = context.entry['build']
    context.entry_before_upsert = context.entry
#    print("before upsert:")
#    print(context.swlist)
    context.swlist.upsert(context.entry)
#    print("after upsert:")
#    print(context.swlist)
    context.entry_after_upsert = context.swlist.find(name=name, build=build)
    
@when(u'the partial entry is upserted to the software list')
def step_impl(context):
    name = context.partial_entry['name']
    build = context.partial_entry['build']
    context.entry_before_upsert = context.entry
    context.swlist.upsert(context.partial_entry)
    context.entry_after_upsert = context.swlist.find(name=name, build=build)

## === 

@given(u'an empty software list')
def step_impl(context):
    filename = 'new_swlist.json'
    context.swlist = swlist.SWList(os.sep.join([context.test_dir,filename]))
    assert len(context.swlist) == 0

@given(u'a software list with an entry for "{name}" "{build}"')
def step_impl(context, name:str, build:str):
    filename = 'sample-list.json'
    context.swlist = swlist.SWList(os.sep.join([context.test_dir,filename]))
    context.initial_swlist_len = len(context.swlist)
    context.entry = context.swlist.find(name=name, build=build)
    # avoid false fails from test data lacking hash field:
    context.entry.update_hash()

@given(u'a valid entry for a Provided software with')
def step_impl(context):
    # start with a sufficiently-complete entry template:
    entry_dict = example_data.valid_provided_entry_dict()
    # and update it according to what the scenario specifies:
    for row in context.table:
        entry_dict[row['field']] = row['value']
    schema = context.swlist.schema
    context.entry = swlist.Entry(schema).compose(entry_dict)
    context.entry.assert_valid()

@given(u'a partial entry with {field} for that same software')
def step_impl(context, field:str):
    name = context.entry['name']
    build = context.entry['build']
    partial_entry_dict = example_data.partial_provided_entry_dict(name, build, field) 
    context.partial_entry = swlist.Entry(context.swlist.schema).compose(partial_entry_dict)

@given(u'the entry has a {field} field')
def step_impl(context, field:str):
    assert field in context.entry

@given(u'the entry has no {field} field')
def step_impl(context, field:str):
    assert field not in context.entry

## === 

# this is used as either a given or a then
@step(u'the software list contains {n} entries')
def step_impl(context, n:str):
    print(len(context.swlist))
    print(context.swlist)
    assert len(context.swlist) == int(n)

@then(u'the list should be valid')
def step_impl(context):
    assert context.swlist.is_valid()

@then(u'the entry after upsert should still contain the fields it had before upsert')
def step_impl(context):
    before = set(context.entry_before_upsert.keys())
    after = set(context.entry_after_upsert.keys())
    assert before.issubset(after), f"\nbefore: {before}\n after: {after}"

@then(u'the entry after upsert should be unchanged except for {field}')
def step_impl(context, field:str):
    before = {k:v for k,v in context.entry_before_upsert.items() if k != field}
    after = {k:v for k,v in context.entry_after_upsert.items() if k != field}
    assert before == after, f"mismatch:\n{before} != \n{after}"
    for key in before:
        assert context.entry_before_upsert[key] == context.entry_after_upsert[key]

@then(u'the entry after upsert should contain the new {field}')
@then(u'the entry after upsert should contain the updated {field}')
def step_impl(context, field):
    assert context.entry_after_upsert[field] == context.partial_entry[field]



#
## Naming conventions:
##   context.unchanged_existing_entry: swlist.Entry
##     an unchanged existing entry found in the swlist
##   context.entry_before_modification: swlist.Entry 
##     an Entry read or formed during the testing setup, that forms the 
##     base-state to which we will apply the modification being tested
##   context.partial_entry: dict
##     a dict holding the modification-under-test to be applied to the
##     entry_before_modification
##   context.entry_before_upsert: swlist.Entry 
##     an Entry to which some modifcation may have been applied, ready
##     to be upserted
##   context.entry_after_upsert: swlist.Entry
##     the entry as read back from the swlist after upsert
#
## yikes, actually a lot of what we want to test is behavior at combine .. 
## might need to make separate features for ingest (ie add new data) and
## update (is modify/combine data)
## for the update feature, we don't actually need to read and write the 
## software list, only setup and update Entry objects
#
## FIXME clean up the naming, currently it is in an incomplete state
## TODO split into update and ingest functions (maybe)
#
## === this is the key action we are testing ===
#
#@when(u'the entry is upserted to the software list')
#def step_impl(context):
#    """ @given steps should create a context.entry_before_upsert to be upserted, and 
#        @then steps can expect to find a context.entry_after_upsert to
#        compare it it
#    """
#    name = context.entry_before_upsert['name']
#    build = context.entry_before_upsert['build']
#    context.swlist.upsert(context.entry_before_upsert)
#    context.entry_after_upsert = context.swlist.find(name=name, build=build)
#    print(f"entry before upsert is {context.entry_before_upsert}")
#    #print(f"entry after upsert is {context.entry_after_upsert}")
#
#@when(u'the entry is forcefully upserted to the software list')
#def step_impl(context):
#    name = context.entry_before_upsert['name']
#    build = context.entry_before_upsert['build']
#    try:
#        context.swlist.upsert(context.entry_before_upsert)
#    except Exception as ex:
#        context.upsert_exception = ex
#        context.swlist.upsert(context.entry_before_upsert, force=True)
#    context.entry_after_upsert = context.swlist.find(name=name, build=build)
#
## === Adding a sufficiently-complete entry to a new, empty list
#
#@given(u'an empty software list')
#def step_impl(context):
#    filename = 'new_swlist.json'
#    context.swlist = swlist.SWList(os.sep.join([context.test_dir,filename]))
#    assert len(context.swlist) == 0
#
#@given(u'a valid entry for a Provided software')
#def step_impl(context):
#    row = example_data.valid_provided_entry()
#    schema = context.swlist.schema
#    context.entry_before_upsert = swlist.Entry(schema).update_from_tablerow(row)
#    context.entry_before_upsert.validate()
#
#@then(u'the software list should contain "{n}" entries')
#def step_impl(context, n:str):
#    assert len(context.swlist) == int(n)
#
#@then(u'the list should be valid')
#def step_impl(context):
#    assert context.swlist.is_valid()
#
## === Common to update tests: 
#
#@given(u'a software list with an entry for "{name}" "{build}"')
#def step_impl(context, name:str, build:str):
#    filename = 'sample-list.json'
#    context.swlist = swlist.SWList(os.sep.join([context.test_dir,filename]))
#    context.initial_swlist_len = len(context.swlist)
#    context.existing = context.swlist.find(name=name, build=build)
#    #print(context.swlist)
#    #print(context.existing)
#
#@given(u'the combined entry is valid')
#def step_impl(context):
#    """ update setups should create a context.partial that the 
#        context.entry will be updated with 
#    """
#    #print(f"existing entry: {context.existing}")
#    #print(f"new partial entry: {context.partial}")
#    schema = context.swlist.schema
#    existing = context.existing
#    context.entry_before_upsert = swlist.entry(schema,existing).update_from_entry(context.partial)
#    context.entry_before_upsert.validate()
#
## === Updating an existing entry:
#
#@given(u'a partial entry with help for that same software')
#def step_impl(context):
#    row = example_data.partial_provided_entry(only=('name','build','short_help'))
#    #print(context.existing)
#    assert all((row[f] == context.existing[f] for f in ['name','build']))
#    context.partial = swlist.Entry(context.swlist.schema).update_from_tablerow(row)
#
#@then(u'the number of entries should be unchanged')
#def step_impl(context):
#    assert context.initial_swlist_len == len(context.swlist)
#
#@then(u'the entry should now include the added partial entry')
#def step_impl(context):
#    a = context.partial
#    b = context.entry_after_upsert
#    for k,v in a.items():
#        assert b[k] == v
#
#@then(u'the entry should show the update time of the partial entry')
#def step_impl(context):
#    a = context.partial
#    b = context.entry_after_upsert
#    assert a['updated'] == b['updated']
#
## === changing the support level
#
#@given(u'a partial entry increasing the support level for that software')
#def step_impl(context):
#    row = example_data.valid_priority_support_record("current_support.")
#    row['name'] = context.existing['name']
#    row['build'] = context.existing['build']
#    # hopefully people will actually put sensible values in datetime field:
#    # for testing, work around pythons datetime mishandling:
#    today = datetime.datetime.now().replace(hour=0, minute=0, second=0, microsecond=0,tzinfo=None)
#    row['current_support.datetime'] = today.isoformat()
#    assert context.existing['current_support']['level'] != 'Priority'
#    context.partial = swlist.Entry(context.swlist.schema).update_from_tablerow(row)
#
#@given(u'the partial entry does not update the current status')
#def step_impl(context):
#    context.partial.pop('current_status', None)
#
#def _when_as_datetime(when:str) -> datetime.datetime:
#    now = datetime.datetime.now()
#    if when == "yesterday":
#        return now + datetime.timedelta(days=-1)
#    elif when == "tomorrow":
#        return now + datetime.timedelta(days=+1)
#    elif when == "next month":
#        return now + dateutil.relativedelta.relativedelta(now, months=1)
#    elif when == "after 6 months":
#        return now + dateutil.relativedelta.relativedelta(now, months=1, days=1)
#    else:
#        raise NotImplementedError(f"test routine doesn't understand {when}")
#
#@given(u'a partial entry with a planned support reduction dated {when}')
#def step_impl(context, when:str):
#    row = example_data.partial_provided_entry(only=('name','build'))
#    w = _when_as_datetime(when).isoformat()
#    row.update(example_data.valid_minimal_support_record(prefix='planned_support.',when=w))
#    context.partial = swlist.Entry(context.swlist.schema).update_from_tablerow(row)
#
#@given(u'that entry has a planned support reduction dated {when}')
#def step_impl(context, when:str):
#    # we'll keep track of the plan to use when enacting it in later tests:
#    w = _when_as_datetime(when).isoformat()
#    context.plan = example_data.valid_minimal_support_record(when=w)
#    plan = {f"planned_support.{k}":v for k,v in context.plan.items()}
#    context.existing.update_from_tablerow(plan)
#
## this warrants an actual capability to cleanly update the record
#@given(u'a partial entry enacting that support reduction')
#def step_impl(context):
#    row = example_data.partial_provided_entry(only=('name','build'))
#    row.update({f"current_support.{k}":v for k,v in context.plan.items()})
#    context.partial = swlist.Entry(context.swlist.schema).update_from_tablerow(row)
#
#@then(u'the entry should have the new current support')
#def step_impl(context):
#    support = context.entry_after_upsert['current_support']
#    assert support == context.partial['current_support']
#
#@then(u'the entry should have the previous level in its past support')
#def step_impl(context):
#    history = context.entry_after_upsert['past_support']
#    quarry = context.existing['current_support']
#    for i in history:
#        if i['datetime'] == quarry['datetime']:
#            assert i == quarry
#            break
#    else:
#        raise KeyError("no matching entry found in history")
#
#@then(u'that entry should have a planned support reduction dated {when}')
#def step_impl(context, when:str):
#    prior_support = swlist.SupportLevel[context.entry_before_upsert['current_support.level']]
#    new_support = swlist.SupportLevel[context.entry_after_upsert['planned_support.level']]
#    assert new_support < prior_support 
#    w = _when_as_datetime(when).isoformat()
#    assert context.entry_after_upsert['planned_support.datetime'] == w
#
#@then(u'the planned support should be cleared')
#def step_impl(context):
#    assert not "planned_support" in context.entry_after_upsert, str(context.entry_after_upsert)
#
#import dateutil.parser
#@then(u'the current support should reflect the enacted change')
#def step_impl(context):
#    new_current = context.entry_after_upsert['current_support']
#    old_plan = context.plan
#    for k in old_plan.keys():
#        # work around false failures due to python's inexcusable datetime mishandling:
#        new = new_current[k]
#        old = old_plan[k]
#        if k=='datetime':
#            # allow a little leeway on exact times
#            old = dateutil.parser.parse(old).replace(minute=0, second=0, microsecond=0,tzinfo=None).isoformat()
#            new = dateutil.parser.parse(new).replace(minute=0, second=0, microsecond=0,tzinfo=None).isoformat()
#            #new = dateutil.parser.parse(new).replace(hour=0, minute=0, second=0, microsecond=0,tzinfo=None).isoformat()
#        assert new == old, f"for key {k}: {new_current[k]} != {old_plan[k]}"
#
#@then(u'an {exception_type} exception should be raised')
#def step_impl(context, exception_type: str):
#    assert context.upsert_exception.__class__.__name__ == exception_type
#
## === adding a status record
#
#
## --
#
#@then(u'the list should be unchanged')
#def step_impl(context):
#    raise NotImplementedError(u'STEP: Then the list should be unchanged')
#
#
#@given(u'a partial entry that will trigger an InsufficientNotice exception')
#def step_impl(context):
#    raise NotImplementedError(u'STEP: Given a partial entry that will trigger an InsufficientNotice exception')
#
