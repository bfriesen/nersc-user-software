#!/usr/bin/env python3

import os
import tinydb as tdb
import swlist
import example_data
import datetime

# === this is the key action we are testing ===

@when(u'headings are requested')
def step_impl(context):
    """ @given steps should 
        @then steps can expect to find 
    """
    schema = swlist.SWList(os.sep.join([context.test_dir,'dummy.json'])).schema
    if context.response_format in ('csv', 'tsv'):
        context.headers = schema.get_flattened_headings(context.fields)
    else:
        context.headers = schema.get_folded_headings(context.fields)

# === Flattened subset of headings for ingesting from a table ===

@given(u'a format selection of "{fmt}"')
def step_impl(context, fmt):
    context.response_format = fmt

@given(u'a field list of "{fields}"')
def step_impl(context, fields):
    context.fields = fields.split(',')

@given(u'no field list')
def step_impl(context):
    context.fields = None

@then(u'a list should be produced matching')
def step_impl(context):
    expected = [row[0] for row in context.table]

    assert context.headers == expected, f"{context.headers} does not match {expected}"

