#!/usr/bin/env python3

import os
import datetime
import shutil
def before_all(context):
    now = datetime.datetime.now().strftime('%Y-%m-%d-%H.%M.%S')
    context.test_dir_base = f'tmp/behave-{now}'
    os.makedirs(context.test_dir_base)

def after_all(context):
    # clean up if the test directory is empty, but if it is not empty, 
    # I probably need to look inside and debug something
    try:
        os.rmdir(context.test_dir_base)
        print(f"removed {context.test_dir_base}")
    except:
        pass

import tempfile
def before_scenario(context, scenario):
    featurename = scenario.filename.rpartition(os.sep)[2].replace('.feature','')
    prefix = f"{featurename}-{scenario.line}."
    context.test_dir = tempfile.mkdtemp(prefix=prefix, dir=context.test_dir_base)
    shutil.copytree('test/', context.test_dir, dirs_exist_ok=True)

import shutil
def after_scenario(context, scenario):
    if (scenario.status == 'passed' and 
        os.path.exists(context.test_dir) and # False):
        not 'debug' in scenario.tags):
        shutil.rmtree(context.test_dir)
        print(f"removed {context.test_dir}")
