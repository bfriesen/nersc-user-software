#!/usr/bin/env python3
""" Unit tests for swlist utility

    The core classes are: 
    - an Entry, which represents an entry in the swlist as a json-compatible 
      nested structure that can be populated from or exported as "table_rows",
      whose column names indicate nesting eg "current_support.level"
    - a Schema, which provides access to the jsonschema information that 
      the Entry can use when importing or exporting records.

    Supporting classes are:
    - a WatermarkedDict
    - a SchemaPropertyInfo, which holds the metadata for a specific property
      (eg the schema might have a property "current_support", which in turn has 
      properties "level" and "datetime", and all three have an attribute 
      called "meta" which holds the type, $specific_type, format, etc)
"""

import pytest
import example_data
import swlist
import dateutil.parser
import datetime

@pytest.fixture
def schema():
    with open("etc/schema.json") as schemafile:
        schema = swlist.Schema(schemafile)
    return schema

@pytest.fixture
def minischema():
    with open("test/mini-schema.json") as schemafile:
        schema = swlist.Schema(schemafile)
    return schema

## Schema tests:

def test_schema_loaded_successfully(schema):
    assert len(schema)>0

def test_direct_properties(schema):
    assert schema['system']['type'] == "string"
    # for each thing we should define a specific type, 
    # which is after dereferencing etc
    assert schema['system']['$specific_type'] == "string"
    assert 'NERSC' in schema['system']['$comment']
    assert schema['help_url']['format'] == "uri"
    assert schema['help_url']['$specific_type'] == "uri"

def test_indirect_properties(schema):
    # the thing itself:
    assert schema['current_support']['type'] == "object"
    assert schema['current_support']['$specific_type'] == "support_record"
    # properties of the thing:
    assert schema['current_support.level']['type'] == "string"
    print(f"current_support.level is {schema['current_support.level']}")
    assert schema['current_support.level']['$specific_type'] == "enum"
    assert "level of support offered" in schema['current_support.level']['$comment']

def test_arrays(schema):
    # simple arrays:
    assert schema['executables']['type'] == "array"
    assert schema['executables']['$specific_type'] == "array[string]"
    assert schema['executables']['items']['type'] == "string"
    assert schema['executables']['items']['$specific_type'] == "string"
    # arrays of objects:
    assert schema['status_history']['type'] == "array"
    assert schema['status_history']['$specific_type'] == "array[status_record]"
    assert schema['status_history']['items']['type'] == "object"
    assert schema['status_history']['items']['$specific_type'] == "status_record"
    # properties of those objects:
    assert schema['status_history.datetime']['type'] == "string"
    assert schema['status_history.datetime']['format'] == "date-time"
    assert schema['status_history.datetime']['$specific_type'] == "date-time"

def test_make_value_schema_compliant(schema):
    path = 'name' ; value = "PETSc" ; expected = value
    assert schema.make_value_schema_compliant(path, value) == expected
    path = 'updated' ; value = "2020/03/02" 
    expected = "2020-03-02T00:00:00-08:00"
    assert schema.make_value_schema_compliant(path, value) == expected
    path = 'software_type' ; value = "HPC application"; expected = value 
    assert schema.make_value_schema_compliant(path, value) == expected
    with pytest.raises(ValueError):
        value = schema.make_value_schema_compliant(path, "not a valid software type")

    path = 'current_support.build' ; value = "@3.12" ; expected = value
    assert schema.make_value_schema_compliant(path, value) == expected
    path = 'current_support.datetime' ; value = "2020/03/02"
    expected = "2020-03-02T00:00:00-08:00"
    assert schema.make_value_schema_compliant(path, value) == expected
    path = 'past_support.datetime' ; value = "2020/03/02"
    expected = "2020-03-02T00:00:00-08:00"
    assert schema.make_value_schema_compliant(path, value) == expected

def test_support_levels(schema):
    "sanity check that the swlist SupportLevel class corresponds to the schema"
    schema_levels = [lvl for lvl in schema['current_support.level']['enum']]
    assert len(schema_levels) == len(swlist.SupportLevel)
    for i,lvl in enumerate(schema_levels):
        assert swlist.SupportLevel[lvl].value == i


def test_flattening(minischema):
    # flattened schema has one element of arrays:
    flat_headers = ["updated", "name", "build", "software_type", 
                    "current_support.datetime", "current_support.level",
                    "current_support.owner", 
                    "past_support.datetime", "past_support.level",
                    "past_support.owner", "executables"]
    pass
#    #print(schema['status_history'])
#    assert schema['status_history']['$type'] == "array"
#    assert schema['status_history']['$specific_type'] == "array[status_record]"
#
#def test_indirect_properties(schema):
#    assert schema['current_support']['$specific_type'] == "support_record"
#    assert schema['current_support.level']['$type'] == "string"
#    assert "level of support offered" in schema['current_support.level']['$comment']
#
#    # even more indirect (within an array)
#    assert schema['status_history.datetime']['$type'] == "string"
#    assert schema['status_history.datetime']['format'] == "date-time"

def test_watermarkedDict():
    d1 = swlist.WatermarkedDict({1:1, 2:2, 3:3})
    # test that copy() works
    d = d1.copy()
    assert list(d.items()) == [(1,1), (2,2), (3,3)]
    d[2] = 3
    assert list(d1.items()) == [(1,1), (2,2), (3,3)]
    # "update" tests:
    def copy_update_test(d2) -> bool:
        d = d1.copy()
        d.watermark()
        d.update(d2)
        return d.changed
    # test d2 changes nothing:
    assert copy_update_test({1:1, 2:2, 3:3}) == False
    # test unchanged subset changes nothing:
    assert copy_update_test({1:1, 2:2}) == False
    # test changed subset changes something 
    assert copy_update_test({1:1, 2:3}) == True
    # test added fields have effect:
    assert copy_update_test({2:2, 3:3, 4:4}) == True
    # sanity check actual contents of dict:
    d2 = {2:2, 3:4, 4:4}
    d = d1.copy()
    d.update(d2)
    assert list(d.items()) == [(1,1), (2,2), (3,4), (4,4)]
    # test setting to same has no effect:
    d = d1.copy()
    d[2] = 2
    assert d.changed == False
    # but setting to different does:
    d = d1.copy()
    d[2] = 3
    assert d.changed == True
    # and so does adding something:
    d = d1.copy()
    d[7] = 7
    assert d.changed == True
    # and setdefault:
    d = d1.copy()
    assert d.setdefault(7,1) == 1
    assert d.changed == True
    # TODO should test del etc too



## Building an Entry from a flat dict

def test_create_simple_entry(schema):
    simple_row = example_data.partial_provided_entry_dict()
    # make sure there is an 'updated' field, since entry will add one if not (kind
    # of testing implementation, but it seems to be the least-inelegant option).
    # same for 'hash' field
    simple_row["updated"] = "2020-05-14"  # also tests date handling
    simple_row['hash'] = swlist.get_hash(simple_row['build'])
    e = swlist.Entry(schema).compose(simple_row)
    assert len(e) == len(simple_row)
    assert e['name'] == simple_row['name']
    # make sure that date field is what jsonschema expects:
    assert e['updated'] == "2020-05-14T00:00:00-07:00"

def test_create_nested_entry(schema):
    row = example_data.partial_provided_entry_dict()
    # make sure there is an 'updated' field, since entry will add one if not (kind
    # of testing implementation, but it seems to be the least-inelegant option).
    # same for 'hash' field
    row["updated"] = "2020-05-14"
    row['hash'] = swlist.get_hash(row['build'])
    flat_keys = set(row.keys())
    rowlen = len(row) # length before adding nested part
    current_support = example_data.valid_minimal_support_record_dict("current_support.")
    row.update(current_support)
    e = swlist.Entry(schema).compose(row)
    assert e['name'] == row['name']
    # the row has more fields, but they should be nested in a single extra field:
    assert flat_keys < e.keys()
    assert flat_keys | {'current_support'} == e.keys()
    assert len(row) == rowlen+len(current_support)
    assert len(e) == rowlen+1
    # and we should now have a field called "current_support" with 2 sub-fields:
    assert len(e['current_support']) == len(current_support)

def test_create_arrayitem_entry(schema):
    # start with a partial (flat) entry:
    row = example_data.partial_provided_entry_dict()
    # make sure there is an 'updated' field, since entry will add one if not (kind
    # of testing implementation, but it seems to be the least-inelegant option).
    # same for 'hash' field
    row["updated"] = "2020-05-14"
    row['hash'] = swlist.get_hash(row['build'])
    rowlen = len(row)
    # and add an array item for status_history:
    stat_rec = example_data.raw_valid_status_record_dict("status_history.")
    row.update(stat_rec)
    print(row)
    e = swlist.Entry(schema).compose(row)
    # we've added only 1 ('status_history') to the previous rowlen:
    assert len(e) == rowlen+1
    # and it should be a single-element array:
    assert len(e['status_history']) == 1
    # but that status_history record has multiple fields:
    assert len(e['status_history'][-1]) == len(stat_rec)
    # also timestamp should have been converted into iso format:
    isotime = example_data.correctly_formatted_valid_status_record_dict()["datetime"]
    assert e['status_history'][-1]['datetime'] == isotime


def test_create_valid_entry(schema):
    " entry that is complete enough to pass validation "
    row = example_data.valid_provided_entry_dict()
    e = swlist.Entry(schema).compose(row)
    e.assert_valid()

def test_entry_update_with_update_time(schema):
    # first create the entry:
    row = example_data.valid_provided_entry_dict()
    e = swlist.Entry(schema).compose(row)
    # now make an update:
    update = example_data.partial_provided_entry_dict()
    update['name'] = row['name']
    update['build'] = row['build']
    update['updated'] = "2020-07-01" # after same data updated time
    u = swlist.Entry(schema).compose(update)
    e.update(u)
    e.assert_valid()
    # and check update time:
    expected = int(dateutil.parser.parse("2020-07-01").timestamp())
    actual = int(dateutil.parser.parse(e['updated']).timestamp())
    assert expected == actual

def test_entry_update_without_update_time(schema):
    """ if an entry is updated, and the update doesn't include 
        then the "updated" field should be set to the current time
    """
    # first create the entry:
    row = example_data.valid_provided_entry_dict()
    marker = dateutil.parser.parse(row['updated']).timestamp()
    e = swlist.Entry(schema).compose(row)
    # now make an update:
    update = example_data.partial_provided_entry_dict()
    update['name'] = row['name']
    update['build'] = row['build']
    u = swlist.Entry(schema).compose(update)
    e.update(u)
    e.assert_valid()
    # and check update time:
    assert dateutil.parser.parse(e['updated']).timestamp() >= marker

def test_entry_as_mapping(schema):
    row = example_data.valid_provided_entry_dict()
    e = swlist.Entry(schema).compose(row)
    for f in ('build', 'current_support.datetime'):
        assert e[f] == schema.make_value_schema_compliant(f,row[f])
    assert 'current_support.level' in e

import fastjsonschema
def test_validate_invalid_entry(schema):
    row = example_data.partial_provided_entry_dict()
    e = swlist.Entry(schema).compose(row)
    with pytest.raises(fastjsonschema.exceptions.JsonSchemaException):
        e.assert_valid()




