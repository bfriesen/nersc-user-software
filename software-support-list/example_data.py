#!/usr/bin/env python3
""" functions that return sample data for use in pytest and behave tests """

_sample_values = {
    'updated': "2020-05-14",
    'name': "matlab",
    'hash': "abcdef",
    'build': "@MCR_R2016a",
    'access_advice': "module",
    'access_url': "cle7/extra_modulefiles/matlab",
    'installed_at': "/global/common/sw/matlab",
    'build_artifacts': "gitlab.com/NERSC/nersc-user-software/matlab",
    'system': "cori haswell",
    'software_type': "HPC application",
    'description': "user-friendly numeric computing environment",
    'short_help': "works best under NX",
    'help_url': "www.mathworks.com",
    'current_support.datetime': "2020-01-11T00:00:00-08:00",
    'current_support.level': "Provided",
    'current_support.justification': "because it is important",
    'current_support.owner': "sleak",
    'current_support.notes': "something something",
    'current_status.datetime': "2020-04-11T13:01:00-07:00",
    'current_status.status': "ProblemReported",
#    'current_status.known_issues':
#    'planned_support.datetime':
#    'planned_support.level':
#    'planned_support.justification':
#    'planned_support.owner':
#    'planned_support.notes':
#    'past_support.datetime':
#    'past_support.level':
#    'past_support.justification':
#    'past_support.owner':
#    'past_support.notes':
#    'status_history.datetime':
#    'status_history.status': 
#    'status_history.known_issues': "some known issue",
    'tests': "ThisTest|ThatTest",
    'executables': "a.out, vasp_std",
    'dependencies.name': "glibc",
    'dependencies.build': "@6.3",
    'notes': "some random note"
}

def valid_minimal_support_record_dict(prefix="", when="2020-01-11"):
    " a simple but sufficiently-complete support record for Minimal software "
    return { f"{prefix}datetime":          when,
             f"{prefix}level":         "Minimal",
             f"{prefix}justification": "blah blah blah"
           }

def valid_provided_support_record_dict(prefix=""):
    " a simple but sufficiently-complete support record for Provided software "
    d = valid_minimal_support_record_dict(prefix)
    d[f"{prefix}level"] = "Provided"
    d[f"{prefix}owner"] = "sleak"
    return d

def valid_priority_support_record_dict(prefix=""):
    d = valid_minimal_support_record_dict(prefix)
    d[f"{prefix}level"] = "Priority"
    d[f"{prefix}owner"] = "sleak"
    return d


def invalid_minimal_support_record_dict(prefix=""):
    d = valid_minimal_support_record_dict(prefix)
    d[f"{prefix}level"] = "NotARealLevel"
    return d

def partial_minimal_support_record_dict(prefix="", when="2020-01-11"):
    " might be an update to an existing support record "
    return { f"{prefix}datetime":          when,
             f"{prefix}justification": "because it is important"
           }

def raw_valid_status_record_dict(prefix="", datetime="2020-04-11 13:01"):
    d = { f"{prefix}datetime":   datetime,
          f"{prefix}status": "ProblemReported"
        }
    return d

def correctly_formatted_valid_status_record_dict(prefix=""):
    # NOTE datetime must match raw_valid_status_record
    d = raw_valid_status_record_dict(prefix)
    d[f"{prefix}datetime"] = "2020-04-11T13:01:00-07:00" 
    return d


def invalid_status_record_dict(prefix=""):
    return { f"{prefix}datetime":   "2020-05-14",
             f"{prefix}status": "NotARealStatus"
           }

def valid_provided_entry_dict(name="matlab", build="@MCR_R2016a", updated="2020-05-14", **other_fields):
    " a simple but sufficiently-complete entry for Provided support software "
    support = valid_provided_support_record_dict(prefix="current_support.")
    status = raw_valid_status_record_dict(prefix="current_status.")
    d = { "updated": updated,
          "name":    name,
          "build":   build
        }
    d.update(support)
    d.update(status)
    d.update(other_fields)
#    d.update( {f"current_support.{k}":v for k,v in support.items()} )
#    d.update( {f"current_status.{k}":v for k,v in status.items()} )
    return d

def partial_provided_entry_dict(name="matlab", build="@MCR_R2016a", *other_fields): 
    " eg to update an existing record. based on the same test data as full valid entry "
    #base = valid_provided_entry_dict(name, build)
    base = {'name': name, 'build':build}
    base.update({f: _sample_values[f] for f in other_fields})
    return base


