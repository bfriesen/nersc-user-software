# NERSC Software Support List

This directory holds NERSC support details for third-party software, in accordance
with our [software support policy](https://docs.nersc.gov/policies/software-policy/).

The list itself is in `data/software-list.json`.

The list is in the form of a JSON file conforming to the schema in `etc/schema.json`.
The file also conforms to the conventions used by 
[TinyDB](https://tinydb.readthedocs.io/en/latest/index.html), and the `swlist.py` 
utility in this directory uses TinyDB to query and update the file. 

The list is simply json - you can search and update it manually, but the swlist.py
utility aims to make it a little easier to find, update and validate (check for 
schema conformance) the list.

To use the utility first setup a conda environment with the necessary packages:
```
conda create --name swlist pip
conda activate swlist
pip install -r requirements.txt
``` 

To run tests etc for development of the utility you need a few extra packages:
```
pip install -r requirements-devel.txt
```

Naming conventions:

Entry
: A top-level item in the software list, represents a specific build of a 
  specific software. Uniquely identified by the name and build fields

Record
: A history of support and status is kept as a list of object within an 
  entry
